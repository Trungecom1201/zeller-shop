<?php
/*
 * Plugin Name: Woocommerce Email a Friend
 * Plugin URI: http://wordpress.org
 * Description: Email product to your friend from product detail page!
 * Version: 1.0.0
 * Author: Prem Tiwari
 * Author URI: http://wordpress.org
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$plugin = plugin_basename(__FILE__); 

include_once('include/general.class.php');

//add_action('wp_head', 'fm_enqueue_style');
add_action( 'wp_enqueue_scripts', 'fm_enqueue_script' );

/**
* Plugin action links.
* Add settings links to the plugins.php page below the plugin name
*/
function fm_plugin_settings_link($links) { 
  $settings_link = '<a href="admin.php?page=wc-settings&tab=settings_tab_refer_friend">Settings</a>'; 
  array_unshift($links, $settings_link); 
  return $links; 
}
add_filter("plugin_action_links_$plugin", 'fm_plugin_settings_link' );

/**
* Add script to front end website
*/
//function fm_enqueue_script() {
//    wp_enqueue_script( 'jquery');
//    wp_enqueue_script('bootstrap-js',plugins_url("js/bootstrap.min.js",__FILE__));	
//}
/**
* Add stylesheet to front end website
*/
//function fm_enqueue_style() {
//    wp_enqueue_style('fm_style',plugins_url("css/bootstrap.min.css",__FILE__));
//}

function fm_add_below_wc_product_addtocart() {
    
    if(get_option('wc_settings_tab_refer_friend')=='yes') {	
        echo '<a href="#" class="button2 alt email_link" data-toggle="modal" data-target="#loginModal">E-mail a Friend</a>';
    }
    
}

add_action( 'woocommerce_single_product_summary', 'fm_add_below_wc_product_addtocart', 40 );

function Email_form()
{

//Get product url
$product_url = 'http://'.$_SERVER['REQUEST_URI'];

$html = '<style type="text/css"> .modal-title {    padding-top: 10px; } .modal-header-primary { color: #fff; padding: 9px 15px; border-bottom: 1px solid #eee;background-color: #EAC930;-webkit-border-top-left-radius: 5px;-webkit-border-top-right-radius: 5px;-moz-border-radius-topleft: 5px;-moz-border-radius-topright: 5px;border-top-left-radius: 5px;border-top-right-radius: 5px;height: 65px;font-size: 25px; } #loginForm button.sendbtn {background: #00364d;    color: #ffffff;} </style>';
$html .= '<div class="container">
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">E-mail a Friend</h5>
            </div>
            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <form id="loginForm" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-xs-4 control-label">Name of recipient</label>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" name="recipientname" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 control-label">*Recipient e-mail address</label>
                        <div class="col-xs-6">
                            <input type="email" class="form-control" name="recipientemail" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 control-label">Your name</label>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" name="yourname" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 control-label">Your e-mail address</label>
                        <div class="col-xs-6">
                            <input type="email" class="form-control" name="youremail" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 control-label">Personal Message</label>
                        <div class="col-xs-6">
                            <textarea class="form-control" rows="5" cols="50" name="message" required>I found this product  and thought you might find it of interest.'.$product_url.'</textarea>                            
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-6 col-xs-offset-3">
                            <button type="submit" name="sendemail" class="button sendbtn alt">E-mail to friend</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>';
    echo $html;
    
/**
* E-mail product details to your friend
*/
    if(isset($_POST['sendemail']))
    {
        $recipientname = $_POST['recipientname'];
        $recipientemail = $_POST['recipientemail'];
        $yourname = $_POST['yourname'];
        $youremail = $_POST['recipientemail'];
        $subject = 'Email from product details';
        
        //set the form headers
        $headers = 'From:<"'.$youremail.'">';
        $headers = 'From: Contact form <prem.tiwari@trantorinc.com>';
        
        // Build the message
        $message  = "Name :Prem Tiwari\n";
        $message .= "Email :trantor.php@gmail.com\n";
        
        wp_mail($recipientname, $subject, $message, $headers);

    }
}

add_action('wp_footer', 'Email_form');

?>