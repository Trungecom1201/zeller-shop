<?php
/**
* Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
* @uses woocommerce_admin_fields()
* @uses self::get_settings()
*/

class WC_Settings_refer_friend {

    public static function init() {
        add_filter( 'woocommerce_settings_tabs_array', __CLASS__ . '::add_settings_tab', 50 );
        add_action( 'woocommerce_settings_tabs_settings_tab_refer_friend', __CLASS__ . '::settings_tab' );
        add_action( 'woocommerce_update_options_settings_tab_refer_friend', __CLASS__ . '::update_settings' );
    }
    
    public static function add_settings_tab( $settings_tabs ) {
        $settings_tabs['settings_tab_refer_friend'] = __( 'Refer A Friend', 'fm_refer_friend' );
        return $settings_tabs;
    }

    /**
     * Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
     * @uses woocommerce_admin_fields()
     * @uses self::get_settings()
     */
    public static function settings_tab() {
        woocommerce_admin_fields( self::get_settings() );
    }

    /**
     * Uses the WooCommerce options API to save settings via the @see woocommerce_update_options() function.
     *
     * @uses woocommerce_update_options()
     * @uses self::get_settings()
     */
    public static function update_settings() {
        woocommerce_update_options( self::get_settings() );
    }

    /**
     * Get all the settings for this plugin for @see woocommerce_admin_fields() function.
     * @return array Array of settings for @see woocommerce_admin_fields() function.
     */
    public static function get_settings() {

        $settings = array(
            'section_title' => array(
                'name'     => __( 'Refer A Friend', 'fm_refer_friend' ),
                'type'     => 'title',
                'desc'     => '',
                'id'       => 'wc_settings_tab_refer_friend'
            ),
            'title' => array(
                'name' => __( 'Enable/Disable', 'fm_refer_friend' ),
                'type'    => 'checkbox',

			    'options' => array(
			      '0'        => __( 'No', 'woocommerce' ),
			      '1'       => __( 'Yes', 'woocommerce' ),
			    ),
                'desc' => __( 'Enable this on product detail page.', 'fm_refer_friend' ),
                'id'   => 'wc_settings_tab_refer_friend'
            ),                       
            'section_end' => array(
                 'type' => 'sectionend',
                 'id' => 'wc_settings_tab_refer_friend_section_end'
            )
        );

        return apply_filters( 'wc_settings_tab_refer_friend_settings', $settings );
    }
}

WC_Settings_refer_friend::init();

?>