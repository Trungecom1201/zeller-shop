<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.fredericgilles.net/fg-magento-to-woocommerce/
 * @since      1.0.0
 *
 * @package    FG_Magento_to_WooCommerce_Premium
 * @subpackage FG_Magento_to_WooCommerce_Premium/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * @package    FG_Magento_to_WooCommerce_Premium
 * @subpackage FG_Magento_to_WooCommerce_Premium/admin
 * @author     Frédéric GILLES
 */
class FG_Magento_to_WooCommerce_Premium_Admin extends FG_Magento_to_WooCommerce_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	public $premium_options = array();			// Options specific for the Premium version
	public $display_multistore = true;			// Display the multistore options
	public $import_selected_store_only = false;	// Import the selected store only
	
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param    string    $plugin_name       The name of this plugin.
	 * @param    string    $version           The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		parent::__construct($plugin_name, $version);
		$this->faq_url = 'https://www.fredericgilles.net/fg-magento-to-woocommerce/faq/';

	}

	/**
	 * Initialize the plugin
	 */
	public function init() {
		$this->deactivate_free_version();
		parent::init();
	}

	/**
	 * Get the Premium options
	 */
	public function get_premium_options() {

		// Default options values
		$this->premium_options = array(
			'meta_keywords_in_tags'		=> false,
			'import_meta_seo'			=> false,
			'url_redirect'				=> false,
			'website'					=> null,
			'store'						=> null,
			'import_customers_orders'	=> 'all',
			'skip_cms'					=> false,
			'skip_products_categories'	=> false,
			'skip_products'				=> false,
			'skip_disabled_products'	=> false,
			'skip_attributes'			=> false,
			'skip_users'				=> false,
			'skip_customers'			=> false,
			'skip_inactive_customers'	=> false,
			'skip_orders'				=> false,
			'skip_reviews'				=> false,
			'skip_coupons'				=> false,
			'skip_redirects'			=> false,
		);
		$this->premium_options = apply_filters('fgm2wcp_post_init_premium_options', $this->premium_options);
		$options = get_option('fgm2wcp_options');
		if ( is_array($options) ) {
			$this->premium_options = array_merge($this->premium_options, $options);
		}
	}

	/**
	 * Deactivate the free version of FG Magento to WooCommerce to avoid conflicts between both plugins
	 */
	private function deactivate_free_version() {
		deactivate_plugins( 'fg-magento-to-woocommerce/fg-magento-to-woocommerce.php' );
	}
	
	/**
	 * Add information to the admin page
	 * 
	 * @param array $data
	 * @return array
	 */
	public function process_admin_page($data) {
		$data['title'] = __('Import Magento Premium', $this->plugin_name);
		$data['description'] = __('This plugin will import product categories, products, images, CMS, users, customers, orders, reviews and coupons from Magento to WooCommerce.<br />Compatible with Magento versions 1.3 to 2.1.', $this->plugin_name);
		$data['description'] .= "<br />\n" . sprintf(__('For any issue, please read the <a href="%s" target="_blank">FAQ</a> first.', $this->plugin_name), $this->faq_url);

		// Premium options
		foreach ( $this->premium_options as $key => $value ) {
			$data[$key] = $value;
		}
		
		// List of web sites
		$websites = get_option('fgm2wc_websites');
		if ( empty($websites) ) {
			$data['websites_options'] = '<option value="0">default</option>';
		} else {
			$data['websites_options'] = $this->format_options($websites, $this->premium_options['website']);
		}
		// List of stores
		$stores = get_option('fgm2wc_stores');
		if ( empty($stores) ) {
			$data['stores_options'] = '<option value="0">default</option>';
		} else {
			$data['stores_options'] = $this->format_options($stores, $this->premium_options['store']);
		}
		$data['display_multistore'] = $this->display_multistore;

		return $data;
	}

	/**
	 * Get the WordPress database info
	 * 
	 * @since 2.0.0
	 * 
	 * @return string Database info
	 */
	public function get_premium_database_info($database_info) {
		// Users
		$count_users = count_users();
		$users_count = $count_users['total_users'];
		$database_info .= sprintf(_n('%d user', '%d users', $users_count, $this->plugin_name), $users_count) . "<br />";

		// Orders
		$orders_count = $this->count_posts('shop_order');
		$database_info .= sprintf(_n('%d order', '%d orders', $orders_count, $this->plugin_name), $orders_count) . "<br />";

		return $database_info;
	}
	
	/**
	 * Save the Premium options
	 *
	 */
	public function save_premium_options() {
		$this->premium_options = array_merge($this->premium_options, $this->validate_form_premium_info());
		update_option('fgm2wcp_options', $this->premium_options);
	}

	/**
	 * Validate POST info
	 *
	 * @return array Form parameters
	 */
	private function validate_form_premium_info() {
		$result = array(
			'meta_keywords_in_tags'		=> filter_input(INPUT_POST, 'meta_keywords_in_tags', FILTER_VALIDATE_BOOLEAN),
			'import_meta_seo'			=> filter_input(INPUT_POST, 'import_meta_seo', FILTER_VALIDATE_BOOLEAN),
			'url_redirect'				=> filter_input(INPUT_POST, 'url_redirect', FILTER_VALIDATE_BOOLEAN),
			'website'					=> filter_input(INPUT_POST, 'website', FILTER_VALIDATE_INT),
			'store'						=> filter_input(INPUT_POST, 'store', FILTER_VALIDATE_INT),
			'import_customers_orders'	=> filter_input(INPUT_POST, 'import_customers_orders', FILTER_SANITIZE_STRING),
			'skip_cms'					=> filter_input(INPUT_POST, 'skip_cms', FILTER_VALIDATE_BOOLEAN),
			'skip_products_categories'	=> filter_input(INPUT_POST, 'skip_products_categories', FILTER_VALIDATE_BOOLEAN),
			'skip_products'				=> filter_input(INPUT_POST, 'skip_products', FILTER_VALIDATE_BOOLEAN),
			'skip_disabled_products'	=> filter_input(INPUT_POST, 'skip_disabled_products', FILTER_VALIDATE_BOOLEAN),
			'skip_attributes'			=> filter_input(INPUT_POST, 'skip_attributes', FILTER_VALIDATE_BOOLEAN),
			'skip_users'				=> filter_input(INPUT_POST, 'skip_users', FILTER_VALIDATE_BOOLEAN),
			'skip_customers'			=> filter_input(INPUT_POST, 'skip_customers', FILTER_VALIDATE_BOOLEAN),
			'skip_inactive_customers'	=> filter_input(INPUT_POST, 'skip_inactive_customers', FILTER_VALIDATE_BOOLEAN),
			'skip_orders'				=> filter_input(INPUT_POST, 'skip_orders', FILTER_VALIDATE_BOOLEAN),
			'skip_reviews'				=> filter_input(INPUT_POST, 'skip_reviews', FILTER_VALIDATE_BOOLEAN),
			'skip_coupons'				=> filter_input(INPUT_POST, 'skip_coupons', FILTER_VALIDATE_BOOLEAN),
			'skip_redirects'			=> filter_input(INPUT_POST, 'skip_redirects', FILTER_VALIDATE_BOOLEAN),
		);
		$result = apply_filters('fgm2wcp_validate_form_premium_info', $result);
		return $result;
	}

	/**
	 * Set the store ID
	 */
	public function set_store_id() {
		if ( $this->display_multistore ) {
			$this->website_id = $this->premium_options['website'];
			$this->store_id = $this->premium_options['store'];
		}
		$this->import_selected_store_only = $this->display_multistore && $this->premium_options['import_customers_orders'] == 'selected_store';
	}
	
	/**
	 * Sets the meta fields used by the SEO by Yoast plugin
	 * 
	 * @param int $new_post_id WordPress ID
	 * @param array $post Magento post or product
	 */
	public function set_meta_seo($new_post_id, $post) {
		if ( $this->premium_options['import_meta_seo'] ) {
			if ( array_key_exists('meta_title', $post) && !empty($post['meta_title']) ) {
				update_post_meta($new_post_id, '_yoast_wpseo_title', $post['meta_title']);
			}
			if ( array_key_exists('meta_description', $post) && !empty($post['meta_description']) ) {
				update_post_meta($new_post_id, '_yoast_wpseo_metadesc', $post['meta_description']);
			}
			if ( array_key_exists('meta_keyword', $post) && !empty($post['meta_keyword']) ) {
				update_post_meta($new_post_id, '_yoast_wpseo_metakeywords', str_replace(array("\n", "\r"), array(',', ''), trim($post['meta_keyword'])));
			}
			if ( array_key_exists('meta_keywords', $post) && !empty($post['meta_keywords']) ) {
				update_post_meta($new_post_id, '_yoast_wpseo_metakeywords', str_replace(array("\n", "\r"), array(',', ''), trim($post['meta_keywords'])));
			}
		}
	}

	/**
	 * Delete the Yoast SEO taxonomy meta data (title, description, keywords)
	 * 
	 * @since 2.32.0
	 */
	public function delete_wpseo_taxonomy_meta() {
		delete_option('wpseo_taxonomy_meta');
	}
	
	/**
	 * Sets the product categories meta fields used by the Yoast SEO plugin
	 * 
	 * @since 2.32.0
	 * 
	 * @param int $new_term_id WordPress term ID
	 * @param array $product_category Magento product category
	 */
	public function set_product_cat_meta_seo($new_term_id, $product_category) {
		if ( $this->premium_options['import_meta_seo'] &&
			( array_key_exists('meta_title', $product_category) ||
			  array_key_exists('meta_description', $product_category) ||
			  array_key_exists('meta_keywords', $product_category)
			)
		) {
			$wpseo_taxonomy_meta = get_option('wpseo_taxonomy_meta');
			if ( array_key_exists('meta_title', $product_category) && !empty($product_category['meta_title']) ) {
				$wpseo_taxonomy_meta['product_cat'][$new_term_id]['wpseo_title'] = $product_category['meta_title'];
			}
			if ( array_key_exists('meta_description', $product_category) && !empty($product_category['meta_description']) ) {
				$wpseo_taxonomy_meta['product_cat'][$new_term_id]['wpseo_desc'] = $product_category['meta_description'];
			}
			if ( array_key_exists('meta_keywords', $product_category) && !empty($product_category['meta_keywords']) ) {
				$wpseo_taxonomy_meta['product_cat'][$new_term_id]['wpseo_metakey'] = $product_category['meta_keywords'];
			}
			update_option('wpseo_taxonomy_meta', $wpseo_taxonomy_meta);
		}
	}
	
	/**
	 * Sets the post tags from the meta keywords
	 *
	 * @since 1.12.0
	 * 
	 * @param int $new_post_id WordPress ID
	 * @param array $post Magento post
	 */
	public function set_post_tags($new_post_id, $post) {
		$this->set_tags($new_post_id, $post, 'post_tag');
	}

	/**
	 * Sets the product tags from the meta keywords
	 *
	 * @since 1.12.0
	 * 
	 * @param int $new_post_id WordPress ID
	 * @param array $product Magento product
	 */
	public function set_product_tags($new_post_id, $product) {
		$this->set_tags($new_post_id, $product, 'product_tag');
	}

	/**
	 * Sets the tags from the meta keywords
	 *
	 * @since 1.12.0
	 * 
	 * @param int $new_post_id WordPress ID
	 * @param array $post Magento post or product
	 * @param string $taxonomy post_tag or product_tag
	 */
	public function set_tags($new_post_id, $post, $taxonomy) {
		if ( $this->premium_options['meta_keywords_in_tags'] ) {
			$meta_keywords = '';
			if (isset($post['meta_keyword']) ) {
				$meta_keywords = $post['meta_keyword'];
			} elseif (isset($post['meta_keywords']) ) {
				$meta_keywords = $post['meta_keywords'];
			}
			$meta_keywords = str_replace(array("\n", "\r"), array(',', ''), trim($meta_keywords));
			$tags = explode(',', $meta_keywords);
			$this->import_tags($tags, $taxonomy);
			
			if ( !empty($tags) ) {
				// Assign the tags to the post
				wp_set_object_terms($new_post_id, $tags, $taxonomy);
			}
		}
	}

	/**
	 * Import tags
	 * 
	 * @since 2.13.0
	 * 
	 * @param array $tags Tags
	 * @param string $taxonomy Taxonomy (post_tag | product_tag)
	 */
	public function import_tags($tags, $taxonomy) {
		foreach ( $tags as $tag ) {
			$new_term = wp_insert_term($tag, $taxonomy);
			if ( !is_wp_error($new_term) ) {
				add_term_meta($new_term['term_id'], '_fgm2wc_imported', 1, true);
			}
		}
	}

	/**
	 * Add a user if it does not exists
	 *
	 * @param string $firstname User's first name
	 * @param string $lastname User's last name
	 * @param string $login Login
	 * @param string $email User's email
	 * @param string $password User's password in Magento
	 * @param int $mg_user_id Magento User ID
	 * @param string $register_date Registration date
	 * @param string $role User's role - default: subscriber
	 * @return int User ID
	 */
	public function add_user($firstname, $lastname, $login, $email, $password, $mg_user_id, $register_date='', $role='subscriber') {
		$display_name = trim($firstname . ' ' . $lastname);
		$email = sanitize_email($email);
		// Login and nickname
		if ( empty($login) ) {
			$login = $email;
			$nickname = $display_name;
		} else {
			$login = FG_Magento_to_WooCommerce_Tools::convert_to_latin(remove_accents($login));
			$login = sanitize_user($login, true);
			$nickname = $login;
		}
		$login = substr($login, 0, 60);
		$user_nicename = substr(str_replace(' ', '', $nickname), 0, 50);
		
		$user = get_user_by('slug', $login);
		if ( !$user ) {
			$user = get_user_by('email', $email);
		}
		if ( !$user ) {
			// Create a new user
			$userdata = array(
				'user_login'		=> $login,
				'user_pass'			=> wp_generate_password( 12, false ),
				'nickname'			=> $nickname,
				'user_nicename'		=> $user_nicename,
				'user_email'		=> $email,
				'display_name'		=> $display_name,
				'first_name'		=> $firstname,
				'last_name'			=> $lastname,
				'user_registered'	=> $register_date,
				'role'				=> $role,
			);
			$user_id = wp_insert_user( $userdata );
			if ( is_wp_error($user_id) ) {
				//$this->display_admin_error(sprintf(__('Creating user %s: %s', $this->get_plugin_name()), $login, $user_id->get_error_message()));
			} else {
				add_user_meta($user_id, '_fgm2wc_old_user_id', $mg_user_id, true);
				if ( !empty($password) ) {
					// Magento password to authenticate the users
					add_user_meta($user_id, 'magentopass', $password, true);
				}
				//$this->display_admin_notice(sprintf(__('User %s created', $this->get_plugin_name()), $login));
			}
		}
		else {
			$user_id = $user->ID;
			global $blog_id;
			if ( is_multisite() && $blog_id && !is_user_member_of_blog($user_id) ) {
				// Add user to the current blog (in multisite)
				add_user_to_blog($blog_id, $user_id, $role);
			}
		}
		return $user_id;
	}

	/**
	 * Recount the terms
	 * 
	 * @since 2.1.0
	 */
	public function recount_terms() {
		$taxonomies = get_taxonomies();
		foreach ( array_keys($taxonomies) as $taxonomy ) {
			$terms = get_terms($taxonomy, array('hide_empty' => false));
			$termtax = array();
			foreach($terms as $term) {
				$termtax[] = $term->term_taxonomy_id; 
			}
			wp_update_term_count($termtax, $taxonomy);
		}
	}
	
	/**
	 * Append the lists of websites and stores to an array
	 * 
	 * @since 2.20.0
	 * 
	 * @param array $result Array
	 * @return array Same array with the websites and stores appended
	 */
	public function append_websites_and_stores($result) {
		$websites_and_stores = $this->get_lists_of_websites_and_stores();
		if ( $websites_and_stores !== false ) {
			$result = array_merge($result, $websites_and_stores);
		}
		return $result;
	}
	
	/**
	 * Get the lists of websites and stores
	 * 
	 * @since 2.20.0
	 * 
	 * @return array [
	 *			websites: string HTML SELECT list of websites
	 *			stores: string HTML SELECT list of stores
	 *			] | false
	 */
	private function get_lists_of_websites_and_stores() {
		$websites = get_option('fgm2wc_websites');
		$stores = get_option('fgm2wc_stores');
		if ( !empty($websites) && !empty($stores) ) {
			return array(
				'websites' => $this->format_options($websites, $this->premium_options['website']),
				'stores' => $this->format_options($stores, $this->premium_options['store']),
			);
		}
		return false;
	}
	
	/**
	 * Update the websites and the stores
	 * 
	 * @since 2.20.1
	 */
	public function update_websites_and_stores() {
		if ( $this->magento_connect() ) {
			$websites = $this->get_websites();
			update_option('fgm2wc_websites', $websites);
			
			$stores = $this->get_stores();
			update_option('fgm2wc_stores', $stores);
			
			if ( empty($this->premium_options['website']) ) {
				$this->premium_options['website'] = $this->get_default_website_id();
			}
			if ( empty($this->premium_options['store']) ) {
				$this->premium_options['store'] = isset($stores[0]['id'])? $stores[0]['id'] : 0;
			}
		}
	}
	
	/**
	 * Get the list of Magento web sites
	 * 
	 * @since 2.16.0
	 * 
	 * @return array List of web sites
	 */
	public function get_websites() {
		$websites = array();
		$prefix = $this->plugin_options['prefix'];
		$website_table = version_compare($this->magento_version, '2', '<')? 'core_website' : 'store_website';

		$sql = "
			SELECT w.website_id AS id, w.code, w.name, w.is_default
			FROM ${prefix}${website_table} w
			ORDER BY w.sort_order
		";
		$websites = $this->magento_query($sql);
		
		return $websites;
	}
	
	/**
	 * Get the list of Magento stores
	 * 
	 * @since 2.2.0
	 * 
	 * @return array List of stores
	 */
	public function get_stores() {
		$stores = array();
		$prefix = $this->plugin_options['prefix'];
		$store_table = version_compare($this->magento_version, '2', '<')? 'core_store' : 'store';
		$store_group_table = version_compare($this->magento_version, '2', '<')? 'core_store_group' : 'store_group';

		$sql = "
			SELECT s.store_id AS id, s.code, s.name, IF(sg.group_id IS NULL, 0, 1) AS is_default
			FROM ${prefix}${store_table} s
			LEFT JOIN ${prefix}${store_group_table} sg ON sg.default_store_id = s.store_id
			WHERE s.is_active = 1
			ORDER BY s.sort_order
		";
		$stores = $this->magento_query($sql);
		
		return $stores;
	}
	
	/**
	 * Print the options of the websites and stores select boxes
	 * 
	 * @since 2.16.0
	 * 
	 * @param array $items Magento items
	 * @param int $selected_value Selected value
	 * @return string Options
	 */
	private function format_options($items, $selected_value) {
		$options = '';
		foreach ( $items as $item ) {
			if ( isset($item['id']) ) {
				$selected = is_null($selected_value)? $item['is_default'] == 1 : $item['id'] == $selected_value;
				$options .= '<option value="' . $item['id'] . '"' . ($selected? 'selected="selected"': '') . '>' . $item['name'] . '</option>' . "\n";
			}
		}
		return $options;
	}
	
	/**
	 * Update the already imported products and orders
	 * 
	 * @since 2.3.0
	 * 
	 * @param string $action Action
	 */
	public function update($action) {
		if ( $action != 'update' ) {
			return;
		}
		
		if ( defined('DOING_CRON') || check_admin_referer( 'parameters_form', 'fgm2wc_nonce') ) { // Security check
			if ( $this->magento_connect() ) {
				$this->entity_type_codes = $this->get_magento_entity_type_codes();
				$this->set_entity_types($this->entity_type_codes);
				$this->product_visibilities = $this->create_woocommerce_product_visibilities(); // (Re)create the WooCommerce product visibilities

				// Hook for doing other actions before the update
				do_action('fgm2wc_pre_update');

				$this->update_products();
				update_option('fgm2wc_last_update', date('Y-m-d H:i:s'));
			}
		}
	}
	
	/**
	 * Update the already imported products
	 * 
	 * @since 2.3.0
	 */
	private function update_products() {
		$this->log(__('Updating products...', $this->plugin_name));
		$updated_products_count = 0;

		$last_update = get_option('fgm2wc_last_update');
		
		$this->attribute_types = $this->get_magento_attributes();
		$this->entity_type_codes = $this->get_magento_entity_type_codes();

		$products = $this->get_updated_products($last_update);
		foreach ( $products as $product ) {
			$product_id = $product['entity_id'];
			
			// Other fields
			$product = array_merge($product, $this->get_other_product_fields($product_id, $this->product_type_id));

			// Stock
			$stock = $this->get_stock($product_id, $this->website_id);
			if ( empty($stock) ) {
				$stock = $this->get_stock($product_id, 0); // Get the stock of the website 0
			}
			$product = array_merge($product, $stock);
			
			// Get the WordPress product ID
			$post_id = $this->get_wp_product_id_from_magento_id($product_id);
			if ( !empty($post_id) ) {

				// Update stock
				$manage_stock = $this->set_manage_stock($product);
				$stock_status = (($product['is_in_stock'] > 0) || ($manage_stock == 'no'))? 'instock': 'outofstock';
				update_post_meta($post_id, '_manage_stock', $manage_stock);
				update_post_meta($post_id, '_stock_status', $stock_status);
				update_post_meta($post_id, '_stock', $product['qty']);
				
				do_action('fgm2wc_post_update_product', $post_id, $product);
				$updated_products_count++;
			}
		}
		$this->display_admin_notice(sprintf(_n('%d product updated', '%d products updated', $updated_products_count, $this->plugin_name), $updated_products_count));

		// Hook for doing other actions after all products are updated
		do_action('fgm2wc_post_update_products');
	}
	
	/**
	 * Get the products updated after a date
	 * 
	 * @since 2.3.0
	 * 
	 * @param date $last_update
	 */
	private function get_updated_products($last_update) {
		$products = array();
		$prefix = $this->plugin_options['prefix'];

		$order_items_table = $this->table_exists('sales_flat_order_item')? 'sales_flat_order_item' : 'sales_order_item';
		$sql = "
			SELECT DISTINCT p.entity_id, p.type_id
			FROM ${prefix}catalog_product_entity p
			INNER JOIN ${prefix}catalog_product_entity_int pei ON pei.entity_id = p.entity_id
			INNER JOIN ${prefix}eav_attribute a ON a.attribute_id = pei.attribute_id
			WHERE p.updated_at > '$last_update'
			AND a.attribute_code = 'visibility'
			AND pei.value != 1 -- Different from 'Not visible individually'

			UNION

			SELECT DISTINCT p.entity_id, p.type_id
			FROM ${prefix}catalog_product_entity p
			INNER JOIN ${prefix}{$order_items_table} oi ON oi.product_id = p.entity_id
			INNER JOIN ${prefix}catalog_product_entity_int pei ON pei.entity_id = p.entity_id
			INNER JOIN ${prefix}eav_attribute a ON a.attribute_id = pei.attribute_id
			WHERE oi.updated_at > '$last_update'
			AND a.attribute_code = 'visibility'
			AND pei.value != 1 -- Different from 'Not visible individually'
		";
		$products = $this->magento_query($sql);

		return $products;
	}
	
	/**
	 * Normalize the attribute name
	 * 
	 * @since 2.5.0
	 * 
	 * @param string $attribute_label Attribute label
	 * @param bool $with_crc_prefix Prefix the name by a CRC (to avoid the duplicates due to the truncation of long labels)
	 * @return string Normalized attribute name
	 */
	public function normalize_attribute_name($attribute_label, $with_crc_prefix = false) {
		$attribute_label = trim($attribute_label);
		if ( $with_crc_prefix ) {
			$crc = hash("crc32b", $attribute_label);
			$short_crc = substr($crc, 0, 2); // Keep only the 2 first characters (should be enough)
		}
		$attribute_label = str_replace(',', '_', $attribute_label); // To avoid duplicates between 1,2 and 12 for example
		$attribute_name = sanitize_key(FG_Magento_to_WooCommerce_Tools::convert_to_latin($attribute_label));
		if ( $with_crc_prefix ) {
			$attribute_name = $short_crc . '-' . $attribute_name;
		}
		$attribute_name = str_replace('pa_', 'paa_', $attribute_name); // Workaround to WooCommerce bug that doesn't process well the attributes containing "pa_". Issue opened on WooCommerce: https://github.com/woocommerce/woocommerce/issues/22101
		$attribute_name = substr($attribute_name, 0, 29); // The taxonomy is limited to 32 characters in WordPress
		return $attribute_name;
	}
	
	/**
	 * Get the WooCommerce products
	 *
	 * @since 2.21.0
	 * 
	 * @return array of products mapped with the Magento products ids
	 */
	public function get_woocommerce_products() {
		global $wpdb;
		$products = array();

		try {
			$sql = "
				SELECT post_id, meta_value
				FROM $wpdb->postmeta
				WHERE meta_key = '_fgm2wc_old_product_id'
			";
			$rows = $wpdb->get_results($sql);
			foreach ( $rows as $row ) {
				$products[$row->meta_value] = $row->post_id;
			}
		} catch ( PDOException $e ) {
			$this->display_admin_error(__('Error:', __CLASS__) . $e->getMessage());
		}
		return $products;
	}

}
