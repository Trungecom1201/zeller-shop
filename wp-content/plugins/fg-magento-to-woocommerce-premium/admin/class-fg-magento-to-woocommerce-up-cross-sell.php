<?php
/**
 * Up Sell and Cross Sell class
 *
 * @link       https://www.fredericgilles.net/fg-magento-to-woocommerce/
 * @since      2.17.0
 *
 * @package    FG_Magento_to_WooCommerce_Premium
 * @subpackage FG_Magento_to_WooCommerce_Premium/admin
 */

if ( !class_exists('FG_Magento_to_WooCommerce_Up_Cross_Sell', false) ) {

	/**
	 * Up Sell and Cross Sell class
	 *
	 * @package    FG_Magento_to_WooCommerce_Premium
	 * @subpackage FG_Magento_to_WooCommerce_Premium/admin
	 * @author     Frédéric GILLES
	 */
	class FG_Magento_to_WooCommerce_Up_Cross_Sell {

		/**
		 * Initialize the class and set its properties.
		 *
		 * @param    object    $plugin       Admin plugin
		 */
		public function __construct( $plugin ) {

			$this->plugin = $plugin;
		}
		
		/**
		 * Import the Up Sell, Cross Sell and related products for every products
		 * 
		 */
		public function import_up_and_cross_sells() {
			$this->plugin->log(__('Importing Up Sell and Cross Sell...', $this->plugin->get_plugin_name()));
			$imported_product_links_count = 0;
			$imported_products = $this->plugin->get_imported_magento_products();
			
			$product_links = $this->get_product_links();
			foreach ( $product_links as $product_link ) {
				if ( isset($imported_products[$product_link['product_id']]) && isset($imported_products[$product_link['linked_product_id']]) ) {
					$wc_product_id = $imported_products[$product_link['product_id']];
					$wc_linked_product_id = $imported_products[$product_link['linked_product_id']];
					
					switch ( $product_link['code'] ) {
						case 'relation':
						case 'up_sell':
							$up_sell_ids = get_post_meta($wc_product_id, '_upsell_ids', true);
							if ( !is_array($up_sell_ids) ) {
								$up_sell_ids = array();
							}
							if ( !in_array($wc_linked_product_id, $up_sell_ids) ) {
								$up_sell_ids[] = $wc_linked_product_id;
								update_post_meta($wc_product_id, '_upsell_ids', $up_sell_ids);
								$imported_product_links_count++;
							}
							break;
							
						case 'cross_sell':
							$cross_sell_ids = get_post_meta($wc_product_id, '_crosssell_ids', true);
							if ( !is_array($cross_sell_ids) ) {
								$cross_sell_ids = array();
							}
							if ( !in_array($wc_linked_product_id, $cross_sell_ids) ) {
								$cross_sell_ids[] = $wc_linked_product_id;
								update_post_meta($wc_product_id, '_crosssell_ids', $cross_sell_ids);
								$imported_product_links_count++;
							}
							break;
					}
				}
			}
			$this->plugin->display_admin_notice(sprintf(_n('%d Up Sell and Cross Sell imported', '%d Up Sell and Cross Sell imported', $imported_product_links_count, $this->plugin->get_plugin_name()), $imported_product_links_count));
		}
		
		/**
		 * Get the products Up Sell and Cross Sell
		 * 
		 * @return array Product links
		 */
		private function get_product_links() {
			$product_links = array();
			$prefix = $this->plugin->plugin_options['prefix'];
			$sql = "
				SELECT pl.product_id, pl.linked_product_id, plt.code
				FROM ${prefix}catalog_product_link pl
				INNER JOIN ${prefix}catalog_product_link_type plt ON plt.link_type_id = pl.link_type_id
				WHERE plt.code IN('relation', 'up_sell', 'cross_sell')
			";
			$product_links = $this->plugin->magento_query($sql);
			
			return $product_links;
		}

	}
}
