<?php
/**
 * Users authentication module
 * Authenticate the WooCommerce users using the imported Magento passwords
 *
 * @link       https://www.fredericgilles.net/fg-magento-to-woocommerce/
 * @since      1.0.0
 *
 * @package    FG_Magento_to_WooCommerce_Premium
 * @subpackage FG_Magento_to_WooCommerce_Premium/public
 */

if ( !class_exists('FG_Magento_to_WooCommerce_Users_Authenticate', false) ) {

	/**
	 * Users authentication class
	 *
	 * @package    FG_Magento_to_WooCommerce_Premium
	 * @subpackage FG_Magento_to_WooCommerce_Premium/public
	 * @author     Frédéric GILLES
	 */
	class FG_Magento_to_WooCommerce_Users_Authenticate {

		/**
		 * Authenticate a user using his Magento password
		 *
		 * @param WP_User $user User data
		 * @param string $username User login entered
		 * @param string $password Password entered
		 * @return WP_User User data
		 */
		public static function auth_signon($user, $username, $password) {
			
			if ( is_a($user, 'WP_User') ) {
				// User is already identified
				return $user;
			}
			
			if ( empty($username) || empty($password) ) {
				return $user;
			}
			
			$wp_user = get_user_by('login', $username); // Try to find the user by his login
			if ( !is_a($wp_user, 'WP_User') ) {
				$wp_user = get_user_by('email', $username); // Try to find the user by his email
				if ( !is_a($wp_user, 'WP_User') ) {
					// username not found in WP users
					return $user;
				}
			}
			
			// Get the imported magentopass
			$magentopass = get_user_meta($wp_user->ID, 'magentopass', true);
			if ( empty($magentopass) ) {
				return $user;
			}
			
			// Authenticate the user using the magento password
			if ( self::auth_magento($password, $magentopass) ) {
				// Update WP user password
				add_filter('send_password_change_email', '__return_false'); // Prevent an email to be sent
				wp_update_user(array('ID' => $wp_user->ID, 'user_pass' => $password));
				// To prevent the user to log in again with his Magento password once he has successfully logged in. The following times, his password stored in WordPress will be used instead.
				delete_user_meta($wp_user->ID, 'magentopass');
				
				return $wp_user;
			}
			
			return $user;
		}
		
		/**
		 * Magento user authentication
		 *
		 * @param string $username User login entered
		 * @param string $password Password entered
		 * @param string $magentopass Password stored in the WP usermeta table
		 * @return bool Is the Magento password good?
		 */
		private static function auth_magento($password, $magentopass) {
			$is_authentication_ok = false;
			list($hashed_magento_password, $salt) = explode(':', $magentopass);
			
			$salted_password = $salt . stripslashes($password);
			if ( strlen($hashed_magento_password) == 64 ) {
				$is_authentication_ok = hash('sha256', $salted_password) == $hashed_magento_password;
			} else {
				$is_authentication_ok = md5($salted_password) == $hashed_magento_password;
			}
			return $is_authentication_ok;
		}
	}
}
