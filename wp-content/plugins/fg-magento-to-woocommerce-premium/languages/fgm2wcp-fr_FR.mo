��    ?        Y         p  E   q  ,   �  &   �       *   $  &   O     v  $   �  "   �  &   �  *   �  &   #     J  "   [     ~     �     �  (   �  "   �       #   +     O  $   i     �     �     �     �  J   �     +	     J	     Q	     h	     �	  H   �	     �	     �	  #   
     6
     N
     c
     z
     �
     �
     �
     �
     �
     �
               5  %   :  �   `  (         I     g     �     �     �  �   �     �     �     �  �  �  j   �  +   �  /        L  '   a  )   �     �  -   �  3   �  .   .  3   ]  "   �     �  1   �               <  )   X  )   �     �  *   �     �  +         >     _      x     �  S   �  (   �          (  (   ?  1   h  ]   �     �       ;   &     b     z     �     �     �     �     �          !     .     I     Z     x  0   ~  �   �  +   �  &   �  6   �     &     D     b  �   z     i     �  (   �                 >      '   
      +      5   =                3             9       6             	         ;         1          7   ?              "   2      !               /       ,                     0   (   #   4       -   )       8                                     *      $   %   :              .   &           <       %d Up Sell and Cross Sell imported %d Up Sell and Cross Sell imported %d attribute imported %d attributes imported %d coupon imported %d coupons imported %d customer %d customers %d customer imported %d customers imported %d option imported %d options imported %d order %d orders %d order imported %d orders imported %d order updated %d orders updated %d product updated %d products updated %d redirect imported %d redirects imported %d review imported %d reviews imported %d user %d users %d user imported %d users imported Don't import the CMS Don't import the coupons Don't import the customers Don't import the customers without order Don't import the disabled products Don't import the orders Don't import the product attributes Don't import the products Don't import the products categories Don't import the redirects Don't import the reviews Don't import the users Error: For any issue, please read the <a href="%s" target="_blank">FAQ</a> first. Grouped products relations set Import Import Magento Premium Import customers and orders: Import meta keywords as tags Import the meta data (title, description and meta keywords) to Yoast SEO Import the store: Import the web site: Importing Up Sell and Cross Sell... Importing attributes... Importing coupons... Importing customers... Importing options... Importing orders... Importing redirects... Importing reviews... Importing users... Meta keywords: Multisites / Multistores: Partial import: Redirect the Magento URLs SEO: Setting grouped products relations... This plugin will import product categories, products, images, CMS, users, customers, orders, reviews and coupons from Magento to WooCommerce.<br />Compatible with Magento versions 1.3 to 2.1. Too many variations (%d) for product #%d Unable to create directory %s Update stocks and orders status Updating orders... Updating products... Users deleted You may need the <a href="%s" target="_blank">WooCommerce Product Add-Ons plugin</a> and the <a href="%s" target="_blank">Product Options add-on</a> to import the Magento options as add-ons instead of as variations. expand / collapse from all stores from the selected store only Project-Id-Version: FG Magento to WooCommerce
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-11-26 12:53+0100
PO-Revision-Date: 2018-11-26 12:53+0100
Last-Translator: F. GILLES
Language-Team: F. GILLES
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e;_n:1,2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Generator: Poedit 2.2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: admin/class-fg-magento-to-woocommerce-admin.php
X-Poedit-SearchPathExcluded-1: admin/class-fg-magento-to-woocommerce-compatibility.php
X-Poedit-SearchPathExcluded-2: admin/partials/actions.php
X-Poedit-SearchPathExcluded-3: admin/partials/behavior.php
X-Poedit-SearchPathExcluded-4: admin/partials/database-info.php
X-Poedit-SearchPathExcluded-5: admin/partials/empty-content.php
X-Poedit-SearchPathExcluded-6: admin/partials/paypal-donate.php
X-Poedit-SearchPathExcluded-7: admin/partials/settings.php
X-Poedit-SearchPathExcluded-8: admin/partials/help-options.tpl.php
X-Poedit-SearchPathExcluded-9: admin/partials/help-instructions.tpl.php
X-Poedit-SearchPathExcluded-10: admin/class-fg-magento-to-woocommerce-progressbar.php
X-Poedit-SearchPathExcluded-11: admin/partials/logger.php
X-Poedit-SearchPathExcluded-12: admin/partials/progress-bar.php
X-Poedit-SearchPathExcluded-13: admin/class-fg-magento-to-woocommerce-modules-check.php
 %d vente additionnelle et vente croisée importée %d ventes additionnelles et ventes croisées importées %d attribut importé %d attributs importés %d code promo importé %d codes promo importés %d client %d clients %d client importé %d clients importés %d option importée %d options importées %d commande %d commandes %d commande importée %d commandes importées %d commande mise à jour %d commandes mises à jour %d produit mis à jour %d produits mis à jour %d redirection importée %d redirections importées %d avis importé %d avis importés %d utilisateur %d utilisateurs %d utilisateur importé %d utilisateurs importés Ne pas importer le CMS Ne pas importer les codes promo Ne pas importer les clients Ne pas importer les clients sans commande Ne pas importer les produits désactivés Ne pas importer les commandes Ne pas importer les attributs des produits Ne pas importer les produits Ne pas importer les catégories de produits Ne pas importer les redirections Ne pas importer les avis Ne pas importer les utilisateurs Erreur : Pour toute question, veuillez d'abord lire la <a href="%s" target="_blank">FAQ</a>. Relations des produits groupés créées Importer Import Magento Premium Importer les clients et les commandes : Importer les meta keywords en tant que mots-clefs Importer les méta données (titre, description et méta mots-clés) vers le plugin Yoast SEO Importer la boutique : Importer le site web : Import des ventes additionnelles et des ventes croisées... Import des attributs... Import des codes promo... Import des clients... Import des options... Import des commandes... Import des redirections... Import des avis... Import des utilisateurs... Mots-clefs : Multisites / Multistores : Import partiel : Rediriger les URLs de Magento SEO : Création des relations des produits groupés... Le plugin va importer les catégories de produit, les produits, les images, le CMS, les utilisateurs, les clients, les commandes, les avis et les codes promo de Magento vers WooCommerce.<br />Compatible avec Magento versions 1.3 à 2.1. Trop de variations (%d) pour le produit #%d Impossible de créer le répertoire %s Mettre à jour les stocks et les statuts des commandes Mise à jour des commandes... Mise à jour des produits … Utilisateurs supprimés Vous avez peut-être besoin du <a href="%s" target="_blank">plugin WooCommerce Product Add-Ons</a> et du <a href="%s" target="_blank">module Product Options</a> pour importer les options Magento comme add-ons plutôt que comme variations. développer / réduire de toutes les boutiques uniquement de la boutique sélectionnée 