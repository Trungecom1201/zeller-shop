<?php
/**
 * Class which handles all conditional logic
 */
class BM_Conditionals {

	/**
	 * BM_Conditionals constructor.
	 */
	public function __construct() {
		$this->current_customer_group = self::get_validated_customer_group();
	}

	/**
	 * Get current user groups for a the current logged in user
	 *
	 * @return void
	 */
	public static function get_validated_customer_group() {

		$user                    = new BM_User();
		$existing_groups         = array();
		$all_customer_groups     = $user->get_all_customer_groups();
		$current_customer_groups = $user->get_current_customer_groups();

		$guest        = get_transient( 'bm_guest_group' );
		$guest_search = get_option( 'deactivate_guest_search' );

		if ( false === $guest && 'on' !== $guest_search ) {

			$guest_slugs = array( 'Gast', 'Gäste', 'Guest', 'Guests', 'gast', 'gäste', 'guest', 'guests' );

			foreach ( $guest_slugs as $slug ) {
				$guest_object = get_page_by_title( $slug, OBJECT, 'customer_groups' );

				if ( ! is_null( $guest_object ) ) {
					set_transient( 'bm_guest_group', $guest_object, 12 * 60 );
				}
			}
		}
		if ( isset( $current_customer_groups ) && ! empty( $current_customer_groups ) ) {
			foreach ( $current_customer_groups as $group ) {

				foreach ( $all_customer_groups as $group_active ) {

					if ( array_key_exists( $group, $group_active ) ) {

						$args            = array(
							'post_type'      => 'customer_groups',
							'posts_per_page' => 1,
							'post_name__in'  => [ $group ],
							'fields'         => 'ids',
						);
						$existing_groups = get_posts( $args );
					}
				}
			}
		}
		if ( isset( $existing_groups ) && ! empty( $existing_groups ) ) {
			return intval( $existing_groups[0] );
		} elseif ( false !== $guest && ! is_user_logged_in() ) {
			return $guest->ID;
		}
	}

	/**
	 * Check if product is activated for group
	 *
	 * @param int $product_id
	 * @return void
	 */
	public function active_price_for_product( $product_id ) {

		$product_in_group = false;

		if ( isset( $this->current_customer_group ) && ! is_null( $this->current_customer_group ) ) {

			$products_customer_group_meta = get_post_meta( $this->current_customer_group, 'bm_products', true );
			$all_products                 = get_post_meta( $this->current_customer_group, 'bm_all_products', true );

			if ( isset( $products_customer_group_meta ) && ! empty( $products_customer_group_meta ) ) {
				$products_customer_group = explode( ',', $products_customer_group_meta );

				if ( isset( $products_customer_group ) && ! empty( $products_customer_group ) ) {

					if ( in_array( $product_id, $products_customer_group ) ) {
						$product_in_group = true;
					}
				}
			} elseif ( 'on' === $all_products ) {
				$product_in_group = true;
			}
		}

		return $product_in_group;
	}

	/**
	 * Check if product from category is activated for group
	 *
	 * @param int $product_id
	 * @return void
	 */
	public function active_price_for_category( $product_id ) {

		$categories_in_group = false;

		if ( isset( $this->current_customer_group ) && ! is_null( $this->current_customer_group ) ) {

			$cat_customer_group_meta = get_post_meta( $this->current_customer_group, 'bm_categories', true );
			$all_products            = get_post_meta( $this->current_customer_group, 'bm_all_products', true );

			if ( isset( $cat_customer_group_meta ) && ! empty( $cat_customer_group_meta ) ) {
				$categories_customer_group = explode( ',', $cat_customer_group_meta );

				if ( isset( $categories_customer_group ) && ! empty( $categories_customer_group ) ) {
					$terms      = wp_get_post_terms( $product_id, 'product_cat' );
					$categories = array();

					if ( isset( $terms ) && ! empty( $terms ) ) {
						foreach ( $terms as $term ) {
							array_push( $categories, $term->term_id );

							if ( ! empty( $term->parent ) ) {
								array_push( $categories, $term->parent );
							}
						}
					}
					$matched_cats = array_intersect( $categories_customer_group, $categories );
					if ( ! empty( $matched_cats ) ) {
						$categories_in_group = true;
					}
				}
			} elseif ( 'on' === $all_products ) {
				$categories_in_group = true;
			}
		}
		return $categories_in_group;
	}

	/**
	 * Check if product is in any customer group
	 *
	 * @param int $product_id
	 * @return boolean
	 */
	public function is_product_in_customer_groups( $product_id ) {

		$args          = array(
			'posts_per_page' => - 1,
			'post_type'      => 'customer_groups',
			'fields'         => 'ids',
		);
		$groups        = get_posts( $args );
		$active_groups = array();

		if ( isset( $groups ) && ! empty( $groups ) ) {
			foreach ( $groups as $group_id ) {

				$products_customer_group_meta = get_post_meta( $group_id, 'bm_products', true );
				$all_products                 = get_post_meta( $group_id, 'bm_all_products', true );
				$cat_customer_group_meta      = get_post_meta( $group_id, 'bm_categories', true );

				if ( 'on' == $all_products ) {
					array_push( $active_groups, $group_id );

				} elseif ( isset( $products_customer_group_meta ) && ! empty( $products_customer_group_meta ) ) {
					$products_customer_group = explode( ',', $products_customer_group_meta );
					foreach ( $products_customer_group as $product ) {
						if ( $product_id == $product ) {
							array_push( $active_groups, $group_id );
						}
					}
				} elseif ( isset( $cat_customer_group_meta ) && ! empty( $cat_customer_group_meta ) ) {

					$categories_customer_group = explode( ',', $cat_customer_group_meta );

					if ( isset( $categories_customer_group ) && ! empty( $categories_customer_group ) ) {
						$terms      = wp_get_post_terms( $product_id, 'product_cat' );
						$categories = array();

						if ( isset( $terms ) && ! empty( $terms ) ) {
							foreach ( $terms as $term ) {
								array_push( $categories, $term->term_id );
							}
						}
						$matched_cats = array_intersect( $categories_customer_group, $categories );
						if ( ! empty( $matched_cats ) ) {
							array_push( $active_groups, $group_id );
						}
					}
				}
			}
		}
		$current_groups = array_unique( $active_groups, SORT_REGULAR );

		return $current_groups;
	}
}

new BM_Conditionals();
