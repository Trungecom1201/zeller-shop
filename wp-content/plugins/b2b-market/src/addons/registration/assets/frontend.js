jQuery(document).ready(function ($) {

    $('#b2b_role').on('change', function () {

        var group = $(this).find(":selected").val();

        if ( group == 'customer' ) {
            $('#b2b_uid_field').css("display", "none");
            $('#b2b_company_registration_number_field').css("display", "none");
        } else {
            if ( ! jQuery.inArray( group, registration.net_tax_groups ) ) {
                $('#b2b_uid_field label span').html('<abbr class="required" title="erforderlich">*</abbr>');
                $('#b2b_uid_field').css("display", "block");
                $('#b2b_company_registration_number_field').css("display", "block");
            }
        }
    });

    $(window).load(function () {
           $('#b2b_role').trigger("change");

           var group = $('#b2b_role').find(":selected").val();

           if ( 1 === registration.net_tax_groups.length && group !== 'customer') {
               $('#b2b_role_field').css("display", "none");
           }
    });
});
