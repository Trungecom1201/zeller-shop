<?php

class BM_Live_Price {

	/**
	 * Returns an instance of BM_Live_Price
	 *
	 * @return object
	 */
	public static function get_instance() {
		$live_price = new BM_Live_Price();
		return $live_price;
	}

	/**
	 * MPDP_Live_Price constructor
	 */
	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'load_assets' ) );
		add_filter( 'woocommerce_get_price_html', array( $this, 'single_product_live_price' ), 100, 2 );
		add_filter( 'woocommerce_variable_price_html', array( $this, 'single_product_live_price' ), 100, 2 );
		add_action( 'woocommerce_before_add_to_cart_form', array( $this, 'add_hidden_id_field' ), 5 );
		add_action( 'wp_ajax_update_live_price', array( $this, 'update_live_price' ) );
		add_action( 'wp_ajax_nopriv_update_live_price', array( $this, 'update_live_price' ) );
		add_filter( 'woocommerce_product_addons_option_price', array( $this, 'update_addon_price' ), 10, 4 );
	}

	/**
	 * Enqueue assets
	 *
	 * @return void
	 */
	public function load_assets() {

		if ( ! is_product() ) {
			return;
		}

		$product = wc_get_product( get_the_id() );

		if ( is_null( $product ) ) {
			return;
		}
		if ( ! is_cart() ) {
			wp_enqueue_script( 'bm-live-price-js', B2B_PLUGIN_URL . '/assets/public/bm-live-price.js', array( 'jquery' ), '1.0', false );

			if ( $product->is_type( 'variable' ) ) {
				wp_localize_script( 'bm-live-price-js', 'ajax', array(
					'ajax_url' => admin_url( 'admin-ajax.php' ),
					'variable' => true,
				) );
			} else {
				wp_localize_script( 'bm-live-price-js', 'ajax', array(
					'ajax_url' => admin_url( 'admin-ajax.php' ),
				) );
			}
		}
	}

	/**
	 * Show single product price based on serverside quantity
	 *
	 * @param string $price current price.
	 * @param object $product current product object.
	 * @return string
	 */
	public function single_product_live_price( $price, $product ) {

		$quantity   = 1;
		$group_id   = BM_Conditionals::get_validated_customer_group();
		$rrp_price = false;
		$tax_input  = get_option( 'woocommerce_prices_include_tax' );

		if ( is_null( $group_id ) ) {
			return $price;
		}

		$group      = get_post( $group_id );
		$calc_guest = get_option( 'activate_calculation_guests', 'off' );
		$tax_type   = get_post_meta( $group_id, 'bm_tax_type', true );

		if ( 'off' === $calc_guest ) {

			$customer_slugs = array( 'Kunde', 'Kunden', 'Customer', 'Customers', 'customer', 'kunde', 'kunden', 'customers' );
			$guest_slugs    = array( 'Gast', 'Gäste', 'Guest', 'Guests', 'gast', 'gäste', 'guest', 'guests' );

			if ( in_array( $group->post_title, $customer_slugs ) || in_array( $group->post_title, $guest_slugs ) ) {
				return $price;
			}
		}

		if ( $product->is_type( 'variable' ) ) {

			foreach ( $product->get_available_variations() as $variation ) {

				$base_prices = BM_Calculation::get_available_group_prices( $variation['variation_id'] );
				$bulk_prices = BM_Calculation::get_available_bulk_prices( $variation['variation_id'], $quantity );

				if ( isset( $base_prices ) && ! empty( $base_prices ) ) {
					foreach ( $base_prices as $base_price ) {
						$prices[] = floatval( $base_price );
					}
				}
				if ( isset( $bulk_prices ) && ! empty( $bulk_prices ) ) {
					foreach ( $bulk_prices as $bulk_price ) {
						$prices[] = floatval( $bulk_price );
					}
				}
			}
			$variation_product = wc_get_product( $variation['variation_id'] );
			$rrp_price         = get_post_meta( $variation_product->get_id(), 'bm_rrp', true );

			if ( 'on' === $tax_type ) {
				if ( isset( $rrp_price ) && ! empty( $rrp_price ) ) {
					$rrp_args  = array( 'price' => $rrp_price );
					$rrp_price = round( wc_get_price_excluding_tax( wc_get_product( $variation['variation_id'] ), $rrp_args ), 2 );
				}
				$args = array( 'price' => min( $prices ) );
				$cheapest_price = round( wc_get_price_excluding_tax( wc_get_product( $variation['variation_id'] ), $args ), 2 );
			} else {
				if ( 'no' === $tax_input ) {
					$args = array( 'price' => min( $prices ) );
					$cheapest_price = round( wc_get_price_including_tax( wc_get_product( $variation['variation_id'] ), $args ), 2 );
				} else {
					$cheapest_price = min( $prices );
				}
			}
		} else {
			$base_prices = BM_Calculation::get_available_group_prices( $product->get_id() );
			$bulk_prices = BM_Calculation::get_available_bulk_prices( $product->get_id(), $quantity );
			$rrp_price   = get_post_meta( $product->get_id(), 'bm_rrp', true );

			if ( isset( $base_prices ) && ! empty( $base_prices ) ) {
				foreach ( $base_prices as $base_price ) {
					$prices[] = floatval( $base_price );
				}
			}

			if ( isset( $bulk_prices ) && ! empty( $bulk_prices ) ) {
				foreach ( $bulk_prices as $bulk_price ) {
					$prices[] = floatval( $bulk_price );
				}
			}

			if ( 'on' === $tax_type ) {
				if ( isset( $rrp_price ) && ! empty( $rrp_price ) ) {
					$rrp_args  = array( 'price' => $rrp_price );
					$rrp_price = round( wc_get_price_excluding_tax( $product, $rrp_args ), 2 );
				}
				$args = array( 'price' => min( $prices ) );
				$cheapest_price = round( wc_get_price_excluding_tax( wc_get_product( $product ), $args ), 2 );
			} else {
				if ( 'no' === $tax_input ) {
					$args = array( 'price' => min( $prices ) );
					$cheapest_price = round( wc_get_price_including_tax( wc_get_product( $product ), $args ), 2 );
				} else {
					$cheapest_price = min( $prices );
				}
			}
		}

		if ( isset( $rrp_price ) && ! empty( $rrp_price ) ) {
			$markup = apply_filters( 'bm_price_html', '<span class="b2b-single-price"><small>' . __( 'RRP', 'b2b-market' ) . ': [rrp]</small><br>[cheapest]</span>' );
			return str_replace( array( '[rrp]', '[cheapest]' ), array( wc_price( $rrp_price ), wc_price( $cheapest_price ) ), $markup );
		} else {
			$markup = apply_filters( 'bm_original_price_html', '<span class="b2b-single-price">[cheapest]</span>' );
			return str_replace( '[cheapest]', wc_price( $cheapest_price ), $markup );
		}
	}

	/**
	 * Add hidden id for js live price
	 *
	 * @return void
	 */
	public function add_hidden_id_field() {
		?>
		<!-- Assumes that you're using Bootstrap -->
		<span id="current_id" style="visibility:hidden;" data-id="<?php echo get_the_id(); ?>"></span>
		<?php
	}

	/**
	 * Update addon price for WooCommerce Product Addons
	 *
	 * @param string $price current price.
	 * @param string $option given options.
	 * @param string $key option key.
	 * @param string $text option description.
	 * @return string
	 */
	public function update_addon_price( $price, $option, $key, $text ) {

		$quantity = 1;

		/* product addons price formatting hack */
		$price = substr( preg_replace( '/\D/', '', $price ), 0, -2 );

		$base_prices = BM_Calculation::get_available_addon_prices( floatval( $price ), get_the_id() );
		$price       = min( $base_prices );

		return wc_price( $price );
	}

	/**
	 * Live update price with ajax
	 *
	 * @return void
	 */
	public function update_live_price() {

		if ( isset( $_POST['id'] ) && ! empty( $_POST['id'] ) && isset( $_POST['qty'] ) && ! empty( $_POST['qty'] ) ) {

			$prices    = array();
			$quantity  = $_POST['qty'];
			$product   = wc_get_product( $_POST['id'] );
			$tax_input = get_option( 'woocommerce_prices_include_tax' );
			$rrp_price = get_post_meta( $_POST['id'], 'bm_rrp', true );

			$group_id = BM_Conditionals::get_validated_customer_group();

			if ( ! isset( $group_id ) || empty( $group_id ) ) {
				return;
			}

			$tax_type    = get_post_meta( $group_id, 'bm_tax_type', true );
			$base_prices = BM_Calculation::get_available_group_prices( $product->get_id() );
			$bulk_prices = BM_Calculation::get_available_bulk_prices( $product->get_id(), $quantity );

			if ( isset( $base_prices ) && ! empty( $base_prices ) ) {
				foreach ( $base_prices as $base_price ) {
					$prices[] = floatval( $base_price );
				}
			}

			if ( isset( $bulk_prices ) && ! empty( $bulk_prices ) ) {
				foreach ( $bulk_prices as $bulk_price ) {
					$prices[] = floatval( $bulk_price );
				}
			}

			if ( 'on' === $tax_type ) {
				if ( isset( $rrp_price ) && ! empty( $rrp_price ) ) {
					$rrp_args      = array( 'price' => $rrp_price );
					$rrp_price = round( wc_get_price_excluding_tax( $product, $rrp_args ), 2 );
				}
				$args           = array( 'price' => min( $prices ) );
				$cheapest_price = round( wc_get_price_excluding_tax( $product, $args ), 2 );
			} else {
				if ( 'no' === $tax_input ) {
					$args = array( 'price' => min( $prices ) );
					$cheapest_price = round( wc_get_price_including_tax( wc_get_product( $product ), $args ), 2 );
				} else {
					$cheapest_price = min( $prices );
				}
			}

			/* send the response */
			$response = array(
				'sucess' => true,
				'id'     => $_POST['id'],
			);

			if ( isset( $rrp_price ) && ! empty( $rrp_price ) ) {
				$markup            = apply_filters( 'bm_price_html', '<span class="b2b-single-price"><small>' . __( 'RRP', 'b2b-market' ) . ': [rrp]</small><br>[cheapest]</span>' );
				$response['price'] = str_replace( array( '[rrp]', '[cheapest]' ), array( wc_price( $rrp_price ), wc_price( $cheapest_price ) ), $markup );
				$response['rrp'] = $rrp_price;
			} else {
				$markup            = apply_filters( 'bm_price_html', '<span class="b2b-single-price">[cheapest]</span>' );
				$response['price'] = str_replace( '[cheapest]', wc_price( $cheapest_price ), $markup );
			}

			print wp_json_encode( $response );
			exit;
		}
	}
}
