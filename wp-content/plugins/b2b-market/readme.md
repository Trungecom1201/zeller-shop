# B2B Market
Contributors: MarketPress
Requires at least: 4.9+
Tested up to: 4.9+
Stable tag: 1.0.2

## Description

WooCommerce und B2B-Shops passen endlich zusammen, erstmals auch im deutschsprachigen Bereich. Verkaufe gleichzeitig an B2B und B2C. Mit individuellen Preisen für unterschiedliche Kunden, Prüfung der USt-ID, Staffelpreisen, erweiterten Rabatten und vielem mehr. Erweitere deine Umsätze auf Geschäftskunden, Endkunden und weitere Zielgruppen - mit B2B Market.

### Features
<https://marketpress.de/shop/plugins/b2b-market/>

## Installation

### Requirements
* WordPress 4.9+*
* PHP 5.6+*
* WooCommerce 3.4+*

# Installation
 * Installieren Sie zuerst WooCommerce
 * Installieren Sie die Standardseiten für WooCommerce (Folgen Sie dazu der Installationsroutine von WooCommerce)
 * Benutzen Sie den installer im Backend, oder

1. Entpacken sie das zip-Archiv
2. Laden sie es in das `/wp-content/plugins/` Verzeichnis ihrer WordPress Installation auf ihrem Webserver
3. Aktivieren Sie das Plugin über das 'Plugins' Menü in WordPress und drücken Sie aktivieren
4. Folgen Sie den Anweisungen des Installationsbildschirms

## Other Notes
# Acknowledgements
Thanks Mike Jolley (http://mikejolley.com/) for supporting us with the WooCommerce core.

# Licence
 GPL Version 3

# Languages
- English (en_US) (default)
- German (de_DE)

## Changelog

= 1.0 =
- Release

= 1.0.1 =
- Staffelpreise je Variante
- Neues Admin-Interface für Staffelpreise in Produkten
- Angepasste Live-Preis-Berechnung für Varianten
- Korrigierte Übersetzungen
- Behandlung von Umlauten und Sonderzeichen in Kundengruppen-Namen
- Steuer-Darstellung bei Netto-Preisen
- Komma-Preise in allen Feldern
- Konditionale Überprüfung für Warenkorb-Rabatte
- Produktauswahl bei langen Listen
- Fallback-Lösung bei riesigem Produktbestand in Select2
- Minifizierung Scripts und Styles
- REST API Bug in Zusammenhang mit Kalkulation

= 1.0.2 =
- Addon: Min-und Max-Mengen je Produkt und Kundengruppe
- Erweiterung und Bugfixes für Migrator (Varianten)
- Bugfixes Windows-Server und Migrator
- Umfassende Performance-Optimierung und neue Einstellung
- Kunden-und Gastgruppe nutz-und importierbar für Anwendung von Regeln für Gäste und normale Kunden
- Option zur Deaktivierung der Whitelist-Funktion für bestimmte Themes
- Shortcode zur konditionalen Ausgabe nach Kundengruppe (z.B. AGB)
- Neues Staffelpreis-Interface zur besseren Eingabe und eine effizientere Speicherung
- Umbenennung von Kundengruppen / Löschen von Kundengruppen / Anpassung von Kundengruppen verbessert
- Support für Staffel-und Gruppenpreise für Ajax- und Mini-Carts
- Bugfixes: Rundungsfehler in Live-Preis-Berechnung
- Bugfixes: Netto / Brutto-Preis-Berechnung
- Versandmethoden WC 3.5 Komatiblität
- Bugfixes: Kompatibilität mit Product Bundles
- Option: Blacklist / Whitelist für Administratoren deaktivieren
- Global und Kundengruppen-Preise auch für Varianten (ohne die Notwendigkeit von Produktwerten)
- Handling von Umlauten und Sonderzeichen in Kundengruppen-Namen verbessert
- Angepasste Sprachdateien für Core und Plugin Improver
- Validierung für Negativwerten in allen Nummerfeldern
- Helper-Funktion zur Migration von 1.0.1 Staffelpreisen zu 1.0.2
