jQuery(document).ready(function( $ ) {
	
    /* ajax request for add to cart */

    var qty_element = '.qty';

    /* avada theme */
    if( $( '.custom-qty' ).length > 0 ) {
        qty_element = '.custom-qty';
    }

    /* erado theme */
    if( $( '.tc' ).length > 0 ) {
      qty_element = '.tc';
  }

    $( qty_element ).on('change', function () {

        var id = $('#current_id').data('id');
        
        if ( true == ajax['variable'] ) {
          id = $('.variation_id').val();
        }

        var qty = $(this).val();

        $.ajax({
          type: 'POST',
          url: ajax.ajax_url,
          data: {'action' : 'update_live_price', 'id': id, 'qty' : qty },
          dataType: 'json',
          success: function(data) {
            /* delete old markup */
            $('.summary .price .woocommerce-Price-amount').remove();
            $('.summary .price del').remove();
            $('.summary .price ins').remove();
            /* remove old b2b price */
            $('.summary .price .b2b-single-price').replaceWith(data['price']);
          }
        });     
    
        return false;
    });
});

