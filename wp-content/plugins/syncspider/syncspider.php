<?php
/*
Plugin Name: SyncSpider Woocommerce
Plugin URI:  https://developer.wordpress.org
Description: Syncspider API addon for Woocommerce
Version:     1.0.5
Author:      Monobunt
Author URI:  https://www.monobunt.com
*/

/*** create syncspider menu in left dhashboard sidebar so user can access it ***/
add_action('admin_menu', 'monobunt_plugin_create_menu');
function monobunt_plugin_create_menu() {
add_menu_page('Syncspider API addon for Woocommerce', 'Syncspider', 'administrator', __FILE__, 'monobunt_settings_page' , plugins_url('/images/icon.png', __FILE__) );
}

function monobunt_settings_page() {
?>
<div class="wrap">
<h1>Syncspyder settings</h1>
</div>
<?php }

/*** REST API CUSTOM FIELDS API V1***/ 
/*** https://example.com/wp-json/wc/v1/syncspider/metadata ***/
add_action( 'rest_api_init', 'syncspider_register_route1' );
function syncspider_register_route1() {
    register_rest_route( 'wc/v1/syncspider', 'metadata', array(
                    'methods' => 'GET',
                    'callback' => 'syncspider_phrase',
                )
            );
}



/*** REST API CUSTOM FIELDS API V2***/ 
/*** https://example.com/wp-json/wc/v2/syncspider/metadata ***/
add_action( 'rest_api_init', 'syncspider_register_route2' );
function syncspider_register_route2() {
    register_rest_route( 'wc/v2/syncspider', 'metadata', array(
                    'methods' => 'GET',
                    'callback' => 'syncspider_phrase',
                )
            );
}

/*** Auto metatags generation ***/
function syncspider_phrase() {	
$args = array(
'post_type' => 'product',
'posts_per_page' => 1,
'orderby' =>'date',
'order' => 'DESC' );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();  //take latest product in database
global $product; 
endwhile;
wp_reset_query();
$meta_values = get_post_meta($loop->post->ID); //get meta data from latest product added on site
$kolikoima =count($meta_values);  //check how much metatags are there
$dataenc = json_decode(file_get_contents(esc_url( home_url( '/' )).'wp-json/wc/v2/syncspider/metatags'), true); //json call of all metatags
$dataenc = file_put_contents('logmeta.txt', implode("\n", array_keys($dataenc)));  //save file with meta tags - only labels
$dataenc = file('logmeta.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES); // make an array of needed meta tags - labels only
$syncoutput = array();
foreach($dataenc as $dataen){	
//exclude these from API
$defaultwoometatags = array("_wc_review_count", "_wc_rating_count", "_wc_average_rating", "_edit_lock", "_edit_last", "_sku", "_sale_price_dates_from","_sale_price_dates_to","total_sales","_tax_status","_tax_class","_manage_stock","_backorders","_sold_individually","_weight","_length","_width","_height","_upsell_ids","_crosssell_ids","_purchase_note","_default_attributes","_virtual","_downloadable","_product_image_gallery","_download_limit","_download_expiry","_stock","_stock_status","_product_attributes","_product_version","_regular_price","_sale_price","_et_pb_post_hide_nav","_et_pb_page_layout","_et_pb_side_nav","_thumbnail_id","_price","_et_pb_use_builder","_et_builder_version","_et_pb_ab_stats_refresh_interval","_et_pb_old_content","_et_pb_enable_shortcode_tracking","_et_pb_custom_css","et_enqueued_post_fonts","_digital","_downloadable_files","_wpml_word_count","_wpml_media_featured","_wpml_media_duplicate","_wpml_location_migration_done","_sale_label","_low_stock_amount","_et_pb_show_page_creation","_wp_old_slug");
if (in_array($dataen, $defaultwoometatags)){}
else{
$daten2= ucwords(str_replace("_", " ", $dataen)); //prepare names without underscore and made them upercase
$daten2= ucwords(str_replace("-", " ", $dataen)); //prepare names without minus sign
$daten2 = ltrim($daten2, ' '); //remove first empty space
$syncoutput[] = array("slug"=>$dataen,"label"=>$daten2);  //FINAL OUTPUT TO JSON CALL
}
}
return $syncoutput;
}

/*** List all metatags ***/ 
/*** https://example.com/wp-json/wc/v2/syncspider/metatags ***/
add_action( 'rest_api_init', 'syncspider_register_route3' );
function syncspider_register_route3() {
    register_rest_route( 'wc/v2/syncspider', 'metatags', array(
                    'methods' => 'GET',
                    'callback' => 'syncspider_phrase2',
                )
            );
}
function syncspider_phrase2() {	
$args = array(
'post_type' => 'product',
'posts_per_page' => 1,
'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
'orderby' =>'date',
'order' => 'ASC' );

$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); global $product; 
endwhile;
wp_reset_query();
$meta_values = get_post_meta($loop->post->ID);
return rest_ensure_response( $meta_values );
}







/*** https://example.com/wp-json/syncspider/v1/wpml ***/
if ( function_exists('icl_object_id') ) { 
add_action( 'rest_api_init', 'syncspider_register_route4' );
function syncspider_register_route4() {
    register_rest_route( 'syncspider/v1', 'wpml', array(
                    'methods' => 'GET',
                    'callback' => 'syncspider_phrase4',
                )
            );
}

function syncspider_phrase4() {	
    global $wpdb;
	$lang='en';
    $lang_data = array();
    $languages = $wpdb->get_results( 
        $wpdb->prepare( 
            "SELECT code, english_name, active, tag, name, default_locale
            FROM {$wpdb->prefix}icl_languages lang
            INNER JOIN {$wpdb->prefix}icl_languages_translations trans
            ON lang.code = trans.language_code
            AND trans.display_language_code=%s"
            ,
            $lang
            )
    );
    foreach($languages as $l){
		if ($l->active==1){
        //$lang_data[$l->code] = array(
		
            //'english_name' => $l->english_name,
            //'active' => $l->active,
       $izlaz4[] =array(        
		'name' => $l->name,
		'code' => $l->tag,
		'locale' => $l ->default_locale,
            
        );
		}
    }

$izlaz41 =array("name"=>"English","code"=>"en_GB");
return rest_ensure_response( $izlaz4 );
}
}