<?php get_header(); ?>

<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix verona">
            			<?php //get_sidebar(); ?>

            <div id="left-area">
            <ul class="products columns-4">

            <?php
                if ( have_posts() ) :
                    while ( have_posts() ) : the_post();
                        $post_format = et_pb_post_format(); ?>

                <?php
                    $thumb = '';

                    $width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

                    $height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
                    $classtext = 'et_pb_post_main_image';
                    $titletext = get_the_title();
                    $thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
                    $thumb = $thumbnail["thumb"];

                    et_divi_post_format_content();
                ?>
                <li class="post-<?php the_ID(); ?>  product type-product status-publish has-post-thumbnail" >

                    <a href="<?php the_permalink();?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                        <span class="et_shop_image">
                            <?php print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height ); ?>
                            <span class="et_overlay"></span>
                        </span>
                        <h2 class="woocommerce-loop-product__title"><?php the_title(); ?></h2>
                    </a> 

                </li>

                <?php	endwhile;?>
                   </ul>
                <?php
                    if ( function_exists( 'wp_pagenavi' ) )
                        wp_pagenavi();
                    else
                        get_template_part( 'includes/navigation', 'index' );
                else :
                    get_template_part( 'includes/no-results', 'index' );
                endif;
            ?>
         

            </div>
            
        
	
            
            
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php

get_footer();
?>