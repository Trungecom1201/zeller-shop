(function($){  
	
// 	$('li.product_cat-neu .neu_ribbon').mouseover(function(){
// 		$(this).parents('li.product_cat-neu').find('.et_overlay').addClass('active');
// 	});
	
// 	$('li.product_cat-neu .neu_ribbon').mouseout(function(){
// 		$(this).parents('li.product_cat-neu').find('.et_overlay').removeClass('active');
// 	})
	
    $(document).ready(function () {
		$('.top-last-section ul li.menu-item-has-children').append('<span class="click_caret"></span>');
		$('.top-last-section ul li .click_caret').on('click', function () {
         $(this).parents('li').toggleClass('open');
         $(this).parents('li').siblings('li').removeClass('open');
     });

		//$('section.up-sells.upsells.products .products').slick({
		//	dots: false,
		//	infinite: true,
		//	arrows: true,
		//	speed: 500,
		//	slidesToShow: 4,
		//	slidesToScroll: 2,
		//	responsive: [
		//		{
		//			breakpoint: 1024,
		//			settings: {
		//				slidesToShow: 3,
		//				slidesToScroll: 2
		//			}
		//		},
		//		{
		//			breakpoint: 768,
		//			settings: {
		//				slidesToShow: 2,
		//				slidesToScroll: 2
		//			}
		//		},
		//		{
		//			breakpoint: 480,
		//			settings: {
		//				slidesToShow: 1,
		//				slidesToScroll: 1
		//			}
		//		}
		//	]
		//});
	 
	 $('<span class="toggle-dropdown"></span>').appendTo('.navigattion-menu ul li.menu-item-has-children');
	$(".navigattion-menu span.toggle-dropdown").click(function(){
		$(this).parent(".menu-item-has-children").find(".sub-menu").slideToggle();
		$(this).parent(".menu-item-has-children").toggleClass("show");
		$(this).parent(".menu-item-has-children").siblings('.menu-item-has-children').find('.sub-menu').slideUp();
		$(this).parent(".menu-item-has-children").siblings('.menu-item-has-children').removeClass("show");
	});
	 
	 $("#toggle-menu").click(function() {
		  $(this).toggleClass("on");
		  $("#menu").slideToggle();
	});
       
    $('.banner-slider').slick({
	  dots: true,
	  infinite: true,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrow:false,
	  autoplay: false,
	  responsive: [
		{
		  breakpoint: 768,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
	  ]
	});
	
	 $(".prod_filter_btn").click(function() {
		  $(this).toggleClass("on");
		  $("#sidebar").slideToggle();
	});
	

        
     $('.other_products_block span.et_shop_image').matchHeight();
	 $('.other_products_block h2.woocommerce-loop-product__title').matchHeight();
	 $('.recent_products_block ul.products li a span.et_shop_image').matchHeight();
  $('.related.products ul.products li h2.woocommerce-loop-product__title').matchHeight();
	 $('.post-type-archive-product ul.products li.product span.et_shop_image').matchHeight();
	 $('.post-type-archive-product ul.products li h2.woocommerce-loop-product__title').matchHeight();
	$('.entry-content ul.products li.product h2.woocommerce-loop-product__title').matchHeight();
	 $('.related ul.products li a span.et_shop_image').matchHeight();
	 $('.tax-product_cat ul.columns-4.products li a span.et_shop_image').matchHeight();
	 $('ul.products li').matchHeight();
    
    $(".entry-summary a.compare").appendTo("li.compare_btn");
    $(".entry-summary a.email_link").appendTo("li.email_btn");
    $(".entry-summary .shortdetail").insertAfter(".entry-summary .product_title");
    $(".entry-summary .price_container").insertBefore(".entry-summary form");
	$(".top-right-part").clone().appendTo(".navigattion-menu");
        
        
    $("#sidebar").insertBefore("#left-area");
    $(".single-product .legacy-itemprop-offers").insertAfter(".single-product .price_container");
    $(".single-product .wgm-info.shipping_de_string").appendTo(".single-product .p2");

	setTimeout(function(){
		$('.product-template-default .brief_Detail.clearfix').insertBefore('.legacy-itemprop-offers .shipping_de.shipping_de_string');
	}, 500);


  
  $(".search-results ").addClass("woocommerce-page");
	
	});
    

	 
	 jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 3) {
            jQuery('.header').addClass("sticky");
        } else {
            jQuery('.header').removeClass("sticky");
        }
    });
    

	jQuery(window).on('load',function(){
		 $(".prod_filter_btn").insertBefore("#sidebar");
	});
	
	setTimeout(function(){ 
		jQuery( ".single-product .brief_Detail.clearfix" ).insertBefore( ".single-product .entry-summary .cart" );
	}, 500);
	

	jQuery('.tax-product_cat .woocommerce-products-header h1').appendTo(jQuery('.tax-product_cat .breadcrumb-heading'));

	jQuery('.tax-product_cat .woocommerce-breadcrumb').appendTo(jQuery('.tax-product_cat .breadcrumb-heading'));
	
	
	
	jQuery('.searchandfilter ul .sf-field-post-meta-wingspan .sf-range-max').change(function(){
		alert('1');
		var inputStt = $(this).val();
		jQuery('.noUi-handle.noUi-handle-upper').text(inputStt);

	});
	$('.sf-field-post-meta-wingspan .sf-meta-range.sf-meta-range-slider .sf-input-range-number.sf-range-max.sf-input-number').change(function () {
		console.log($('.sf-field-post-meta-wingspan .sf-meta-range.sf-meta-range-slider .sf-input-range-number.sf-range-max.sf-input-number').val());
		return false;
	});

	// Change text

	var getText = $('.product-template-default .wgm-info.woocommerce-de_price_taxrate').text();

	var a = getText.replace("Enthält", "Inkl.");

	$('.product-template-default .wgm-info.woocommerce-de_price_taxrate').text(a);





	setTimeout(function(){
		var quicklinks = $('.textwidget .facetwp-type-slider');

		quicklinks.each(function(){
			var text = $(this).find('.noUi-base').length;
			if(text > 0){
		   		$(this).show();
				$(this).prev().show();
			}
			else {
				$(this).hide();
				$(this).prev().hide();
			}
		});

		//var fillterCate = $('.textwidget .facetwp-type-dropdown');

		//fillterCate.each(function(){
		//	var option = $(this).find('select option:nth-child(2)').length;
        //
		//	if(option > 0){
		//		$(this).show();
		//		$(this).prev().show();
		//	}
		//	else {
		//		$(this).hide();
		//		$(this).prev().hide();
		//	}
		//});

		
		$.each(FWP.settings.num_choices, function(key, val) {
			var $parent = $('.facetwp-facet-' + key);
			(0 === val) ? $parent.hide() : $parent.show();
			(0 === val) ? $parent.prev().hide() : $parent.prev().show();
		});


		var selectSort = $('.textwidget .facetwp-facet.facetwp-type-dropdown');

		selectSort.each(function(){
			var list, i, switching, b, shouldSwitch;
			list = $(this).find('select');
			switching = true;

			while (switching) {

				switching = false;
				b = $(this).find('option');

				for (i = 0; i < (b.length - 1); i++) {

					shouldSwitch = false;

					if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {

						shouldSwitch = true;
						break;
					}
				}
				if (shouldSwitch) {

					b[i].parentNode.insertBefore(b[i + 1], b[i]);
					switching = true;
				}
			}

			var textOption = $(this).find('option');
				textOptionFirst = $(this).find('option:first-child');

			textOption.each(function(){
				var a = $(this).text();

				if(a == "Alle"){
					$(this).insertBefore(textOptionFirst);
				}
			});

		});


		var checkBox = $('.textwidget .facetwp-type-checkboxes .facetwp-overflow .facetwp-checkbox');

		checkBox.each(function(){
			$(this).appendTo('.textwidget .facetwp-type-checkboxes');
		});


		//var list, i, switching, b, shouldSwitch;
		//list = $('.textwidget .facetwp-facet-kategorie');
		//switching = true;
        //
		//while (switching) {
        //
		//	switching = false;
		//	b = list.find('div');
        //
		//	for (i = 0; i < (b.length - 1); i++) {
        //
		//		shouldSwitch = false;
        //
		//		if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {
        //
		//			shouldSwitch = true;
		//			break;
		//		}
		//	}
		//	if (shouldSwitch) {
        //
		//		b[i].parentNode.insertBefore(b[i + 1], b[i]);
		//		switching = true;
		//	}
		//}

	}, 500);

	setTimeout(function(){
		var noUiFacet = $('.textwidget .facetwp-type-slider');

		noUiFacet.each(function(){

			var max = $(this).find('.noUi-handle-upper').attr('aria-valuemax');
			var min = $(this).find('.noUi-handle-upper').attr('aria-valuemin');
			var dataName = $(this).attr('data-name');

			var maxFormat = parseFloat(Intl.NumberFormat().format(max));
			var minFormat = parseFloat(Intl.NumberFormat().format(min));



			if(dataName == 'spannweite'){
				$(this).find('.facetwp-slider').append('<div class="max-facetwp">' + Intl.NumberFormat().format(max) + 'mm' + '</div>');
				$(this).find('.facetwp-slider').append('<div class="min-facetwp">' + Intl.NumberFormat().format(min) + 'mm' + '</div>');
			}
			else if(dataName == 'stellkraft') {
				$(this).find('.facetwp-slider').append('<div class="max-facetwp">' + max + 'kg' + '</div>');
				$(this).find('.facetwp-slider').append('<div class="min-facetwp">' + min + 'kg' + '</div>');
			}
			else {
				$(this).find('.facetwp-slider').append('<div class="max-facetwp">' + '€ ' + Intl.NumberFormat().format(max) + '</div>');
				$(this).find('.facetwp-slider').append('<div class="min-facetwp">' + '€ ' + Intl.NumberFormat().format(min) + '</div>');
			}
		});

		//jQuery('.facetwp-facet.facetwp-facet-marke_hersteller.facetwp-type-dropdown .facetwp-dropdown option').each(function(){
		//	var text = jQuery(this).text();
		//	if (text == 'Any'){
		//		jQuery(this).text('Alle Marke');
		//	}
		//});
        //
		//var a = jQuery('#sidebar .facetwp-type-dropdown');
		//a.each(function(){
		//	var d = jQuery(this).attr('data-name');
		//	var b = jQuery(this).find('.facetwp-dropdown option');
		//	b.each(function(){
		//		var c = jQuery(this).text();
		//		if(c == 'Any'){
		//			var text = 'Alle'+ ' ' + d;
		//			jQuery(this).text(text);
		//		}
		//	});
		//});



	}, 500);





// Click reset show filter
//==========================================================

	$( "body" ).delegate( ".facetwp-facet .facetwp-slider-reset", "click", function() {

		var quicklinks = $('.textwidget .facetwp-type-slider');

		quicklinks.each(function(){
			var text = $(this).find('.noUi-base').length;
			if(text < 0){
				$(this).hide();
				$(this).prev().hide();
			} else {
				$(this).show();
				$(this).prev().show();
			}
		});

		//var fillterCate = $('.textwidget .facetwp-type-dropdown');

		//fillterCate.each(function(){
		//	var option = $(this).find('select option:nth-child(2)').length;
        //
		//	if(option < 0){
		//		$(this).hide();
		//		$(this).prev().hide();
		//	} else {
		//		$(this).show();
		//		$(this).prev().show();
		//	}
		//});

		$.each(FWP.settings.num_choices, function(key, val) {
			var $parent = $('.facetwp-facet-' + key);
			(0 === val) ? $parent.hide() : $parent.show();
			(0 === val) ? $parent.prev().hide() : $parent.prev().show();
		});

	});

// Ajax filter Stop
//=====================================================================

	$( document ).ajaxStop(function() {

		setTimeout(function(){

			//jQuery('.facetwp-facet.facetwp-facet-marke_hersteller.facetwp-type-dropdown .facetwp-dropdown option').each(function(){
			//	var text = jQuery(this).text();
			//	if (text == 'Any'){
			//		jQuery(this).text('Alle Marke');
			//	}
			//});
            //
			//var a = jQuery('#sidebar .facetwp-type-dropdown');
			//a.each(function(){
			//	var d = jQuery(this).attr('data-name');
			//	var b = jQuery(this).find('.facetwp-dropdown option');
			//	b.each(function(){
			//		var c = jQuery(this).text();
			//		if(c == 'Any'){
			//			var text = 'Alle'+ ' ' + d;
			//			jQuery(this).text(text);
			//		}
			//	});
			//});
			

			var checkBox = $('.textwidget .facetwp-type-checkboxes .facetwp-overflow .facetwp-checkbox');

			checkBox.each(function(){
				$(this).appendTo('.textwidget .facetwp-type-checkboxes');
			});

			// sap xep abc

			//var list, i, switching, b, shouldSwitch;
			//list = $('.textwidget .facetwp-facet-kategorie');
			//switching = true;
            //
			//while (switching) {
            //
			//	switching = false;
			//	b = list.find('div');
            //
			//	for (i = 0; i < (b.length - 1); i++) {
            //
			//		shouldSwitch = false;
            //
			//		if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {
            //
			//			shouldSwitch = true;
			//			break;
			//		}
			//	}
			//	if (shouldSwitch) {
            //
			//		b[i].parentNode.insertBefore(b[i + 1], b[i]);
			//		switching = true;
			//	}
			//}

		}, 500);

		var quicklinks = $('.textwidget .facetwp-type-slider');

		quicklinks.each(function(){

			var text = $(this).find('.noUi-base').length;
			if(text > 0){
				$(this).show();
				$(this).prev().show();
			}
			else {
				$(this).hide();
				$(this).prev().hide();
			}
		});

		//var fillterCate = $('.textwidget .facetwp-type-dropdown');

		//fillterCate.each(function(){
		//	var option = $(this).find('select option:nth-child(2)').length;
        //
		//	if(option > 0){
		//		$(this).show();
		//		$(this).prev().show();
		//	}
		//	else {
		//		$(this).hide();
		//		$(this).prev().hide();
		//	}
		//});

		$.each(FWP.settings.num_choices, function(key, val) {
			var $parent = $('.facetwp-facet-' + key);
			(0 === val) ? $parent.hide() : $parent.show();
			(0 === val) ? $parent.prev().hide() : $parent.prev().show();
		});

		var noUiFacet = $('.textwidget .facetwp-type-slider');

		noUiFacet.each(function(){

			var max = $(this).find('.noUi-handle-upper').attr('aria-valuemax');
			var min = $(this).find('.noUi-handle-upper').attr('aria-valuemin');
			var dataName = $(this).attr('data-name');

			var maxFormat = parseFloat(Intl.NumberFormat().format(max));
			var minFormat = parseFloat(Intl.NumberFormat().format(min));

			var a = $(this).find('.facetwp-slider .max-facetwp');

			if(dataName == 'spannweite'){
				if(!a.hasClass('max-facetwp')){
					$(this).find('.facetwp-slider').append('<div class="max-facetwp">' + Intl.NumberFormat().format(max) + 'mm' + '</div>');
					$(this).find('.facetwp-slider').append('<div class="min-facetwp">' + Intl.NumberFormat().format(min) + 'mm' + '</div>');
				}
			}
			else if(dataName == 'stellkraft') {
				if(!a.hasClass('max-facetwp')) {
					$(this).find('.facetwp-slider').append('<div class="max-facetwp">' + max + 'kg' + '</div>');
					$(this).find('.facetwp-slider').append('<div class="min-facetwp">' + min + 'kg' + '</div>');
				}
			}
			else {
				if(!a.hasClass('max-facetwp')) {
					$(this).find('.facetwp-slider').append('<div class="max-facetwp">' + '€ ' + Intl.NumberFormat().format(max) + '</div>');
					$(this).find('.facetwp-slider').append('<div class="min-facetwp">' + '€ ' + Intl.NumberFormat().format(min) + '</div>');
				}
			}

		});

		var selectSort = $('.textwidget .facetwp-facet.facetwp-type-dropdown');

		selectSort.each(function(){
			var list, i, switching, b, shouldSwitch;
			list = $(this).find('select');
			switching = true;

			while (switching) {

				switching = false;
				b = $(this).find('option');

				for (i = 0; i < (b.length - 1); i++) {

					shouldSwitch = false;

					if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {

						shouldSwitch = true;
						break;
					}
				}
				if (shouldSwitch) {

					b[i].parentNode.insertBefore(b[i + 1], b[i]);
					switching = true;
				}
			}

			var textOption = $(this).find('option');
			textOptionFirst = $(this).find('option:first-child');

			textOption.each(function(){
				var a = $(this).text();

				if(a == "Alle"){
					$(this).insertBefore(textOptionFirst);
				}
			});
		});

	});


//=======================================================


})(jQuery);

