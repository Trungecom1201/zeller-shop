<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php
	elegant_description();
	elegant_keywords();
	elegant_canonical();

	/**
	 * Fires in the head, before {@see wp_head()} is called. This action can be used to
	 * insert elements into the beginning of the head before any styles or scripts.
	 *
	 * @since 1.0
	 */
	do_action( 'et_head_meta' );

	$template_directory_uri = get_template_directory_uri();
?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600,700&display=swap" rel="stylesheet">
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
	<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php wp_head(); ?>

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

</head>
<body <?php body_class(); ?>>
    
 
    
<div class="wrapper">
<!--header-start-->
<header class="header">
	<div class="top-part clearfix">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
					<div class="logo">
                        
                        <?php
                        $logo = ( $user_logo = et_get_option( 'divi_logo' ) ) && '' != $user_logo
                        ? $user_logo
                        : $template_directory_uri . '/images/logo.png';

                        ob_start();

                        $et_phone_number = $et_secondary_nav_items->phone_number;
                        $et_email = $et_secondary_nav_items->email; 
                        
                        global $woocommerce;
                        $cart_url = $woocommerce->cart->get_cart_url();
                        
                        $slide_nav = wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'echo' => false, 'items_wrap' => '%3$s' ) );
                        $slide_nav_top = wp_nav_menu( array( 'theme_location' => 'secondary-menu', 'container' => '', 'fallback_cb' => '', 'echo' => false, 'items_wrap' => '%3$s' ) );

                        ?>
  
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						  <img src="<?php echo esc_attr( $logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
					    </a>
                        
					</div>
				</div>
				<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
					<div class="top-right-part clearfix">
						<div class="top-middle-section">
							<ul>
							<?php if ( '' !== ( $et_phone_number = et_get_option( 'phone_number' ) ) ) : ?>                        
                                <li><span><i class="fa fa-phone" aria-hidden="true"></i></span>
                                    <?php echo et_sanitize_html_input_text( $et_phone_number ); ?>
                                </li>
				            <?php endif; ?>                                
                                
                            <?php if ( '' !== ( $et_email = et_get_option( 'header_email' ) ) ) : ?>                        
                        	<li><a href="<?php echo esc_attr( 'mailto:' . $et_email ); ?>"><span><i class="fa fa-envelope" aria-hidden="true"></i></span><?php echo esc_html( $et_email ); ?></a></li>
				            <?php endif; ?>
                                <li><a href="/oeffnungszeiten/" target="_self"><span><i class="fa fa-clock-o" aria-hidden="true"></i></span>Öffnungszeiten</a></li>
							</ul>
						</div>
						<div class="top-last-section">
                            <ul> <?php echo( $slide_nav_top );?>
								<li><a href="<?php echo $cart_url; ?>"><span><i class="fa fa-shopping-cart" aria-hidden="true"></i></span>Warenkorb (<?php echo WC()->cart->get_cart_contents_count();?> Artikel)</a>
                                </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="main-menu">
		<div class="container ">
			<div class="row">
				<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-md-offset-2 col-lg-offset-2">
					<div id="toggle-menu">
						<span class="one"></span>
						<span class="two"></span>
						<span class="three"></span>
					</div>
						<div class="navigattion-menu" id="menu">
                           <ul> <?php echo( $slide_nav );?></ul>
                           
						</div>
						<div class="top-search-part">                            
                            
                         <form role="search" method="get" class="" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <?php
                                printf( '<label><input type="text" class="" placeholder="%1$s" value="%2$s" name="s" title="%3$s" autocomplete="off" /></label>',
                                    esc_attr__( '', 'Divi' ),
                                    get_search_query(),
                                    esc_attr__( 'Search for:', 'Divi' )
                                );
                            ?>
                         </form>
                            
						</div>
				</div>
					
			</div>
		</div>
	</div>
	
</header>
<!--header-end-->

<!-- content-area start -->
	<div class="content-area">
			


<?php if(is_shop()) {?>
<?php echo do_shortcode('[shop_banner_slider]');?>

<?php } ?>