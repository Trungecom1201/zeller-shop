<?php
/**
 * Fires after the main content, before the footer is output.
 *
 * @since 3.10
 */
?>
	</div>

<!--footer-start-->
	<footer>
		<div class="bottom-section">
			<div class="container">
				<div class="row item-link-footer">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="footer-left-section">
							<h4><?php the_field('footer_menu1_title','option');?></h4>
                            <?php wp_nav_menu(array("menu"=>"footer_shop_menu","menu_class"=>"","container"=>""));?>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="footer-left-section footer-middle-section">
							<h4><?php the_field('footer_menu2_title','option');?></h4>
                             <?php wp_nav_menu(array("menu"=>"footer_links_menu","menu_class"=>"","container"=>""));?>

						</div>
					</div>

                    <?php if(get_field('card_list','option')):  ?>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="footer-logo-section clearfix">
							<ul>
                                <?php   while(the_repeater_field('card_list','option')): ?>
								<li><a href="<?php the_sub_field('add_link');?>"><img src="<?php the_sub_field('add_logo');?>"/></a></li>
								<?php endwhile; ?>
							</ul>
						</div>
					</div>
                    <?php endif; ?>


                    <?php if(get_field('logo_list','option')): ?>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="footer-right-section">
							<ul>
                                <?php   while(the_repeater_field('logo_list','option')): ?>
								<li><a href="<?php the_sub_field('add_link');?>"><img src="<?php the_sub_field('add_logo');?>"/></a></li>
								<?php endwhile; ?>
							</ul>
						</div>
					</div>
                    <?php endif; ?>
				</div>
				<div class="row footer-infor">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="parah-section">
							<p><?php the_field('vat_text','option');?></p>
						</div>
					</div>
				</div>
				<div class="row-copyright-section">
					<div class="copyright-section">
						<p><?php the_field('copyright_text','option');?></p>
					</div>
				</div>
			</div>
		</div>
	</footer>
<!--footer-end-->

</div>
<!-- SET: SCRIPTS -->

<?php
//global $searchandfilter;
//$sf_current_query = $searchandfilter->get(20330)->current_query();
//echo "<pre>";print_r($sf_current_query);
?>

	<?php wp_footer(); ?>


</body>
</html>
