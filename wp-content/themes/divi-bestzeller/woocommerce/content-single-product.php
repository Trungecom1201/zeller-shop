<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;
 global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

<div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>

	<?php
		/**
		 * Hook: woocommerce_before_single_product_summary.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary">


		<?php
			/**
			 * Hook: woocommerce_single_product_summary.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 * @hooked WC_Structured_Data::generate_product_data() - 60
			 */
			do_action( 'woocommerce_single_product_summary' );
		?>
		

       <div class="price_container">


           <?php if( $product->is_on_sale() ) { ?>
        <h3> <span style="color: #ff0000;"> jetzt nur <?php $pro = $product->get_sale_price();echo get_woocommerce_currency_symbol(); echo " "; echo number_format ($pro , 2 , ',' , ','); ?> </span></h3>
        <h4>  <del><?php echo "statt "; $price= $product->get_regular_price();   echo get_woocommerce_currency_symbol();echo " ";  echo number_format ($price , 2 , ',' , ','); ?> </del>  </h4>
  <?php   } else {  ?>
<h3> <span style="color: #ff0000;"> <?php  $price= $product->get_regular_price();   echo   number_format ($price , 2 , ',' , ',');echo " "; echo get_woocommerce_currency_symbol()?>    </span></h3>
<?php }?>

       </div>
       <div style="clear:both;">
         <?php echo do_shortcode('[bulk-price-table]');?>
       </div>
        <div class="brief_Detail clearfix">
         <div class="col6 col6_1">
<!--              <p> <?php   if ( !$product->is_in_stock() ) {  echo '<a href="'.get_the_permalink().'" class="button2">' . __( 'SOLD OUT', 'woocommerce' ) . '</a>';    } else { echo "Verfügbarkeit: <strong> Auf Lager </strong> "; } ?></p> -->
			 <p> <?php   if ( !$product->is_in_stock() ) {     } else { echo "Verfügbarkeit: <strong> Auf Lager </strong> "; } ?></p>
             <p> <?php $delivery_time = get_field('delivery_time'); if($delivery_time) {echo "<p>Lieferzeit :</p> <strong>".$delivery_time."</strong>";} ?> </p>
         </div>
            <div class="col6 col6_2">
                <ul>
                    <li><?php echo do_shortcode('[ti_wishlists_addtowishlist]');?></li>
                    <li class="compare_btn"> <img src="<?php echo get_stylesheet_directory_uri();?>/images/compare.png"> </li>
                    <li class="email_btn"> <img src="<?php echo get_stylesheet_directory_uri();?>/images/mail.png">  </li>
                </ul>
            </div>
        </div>



        <div class="test">
<?php
$shipping_class_id = $product->get_shipping_class_id();
$shipping_class= $product->get_shipping_class();
$fee = 0;

if ($shipping_class_id) {
$flat_rates = get_option("woocommerce_flat_rates");
$fee = $flat_rates[$shipping_class]['cost'];
}

$flat_rate_settings = get_option("woocommerce_flat_rate_settings");
//print_r($flat_rate_settings);
//echo 'Shipping cost: ' . ($flat_rate_settings['cost_per_order'] + $fee);




$shipping_class = $product->get_shipping_class();
$flat_rate = new WC_Shipping_Flat_Rate;
$symbol = get_woocommerce_currency_symbol();
//echo $symbol . $flat_rate->flat_rates[$shipping_class]['cost'];
?>

<?php //echo do_shortcode('[display_tax]');?>
        </div>


	</div>

	<?php
		/**
		 * Hook: woocommerce_after_single_product_summary.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
