<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php wc_product_class(); ?>>


<a href="<?php the_permalink(); ?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
<?php //do_action( 'woocommerce_before_shop_loop_item_title' );?>

 <span class="et_shop_image">
        <?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); if($feat_image){?>
            <img src="<?php echo $feat_image; ?>" alt="image" class="">
        <?php } ?>
        <span class="et_overlay"></span>
    </span>

    
    <h2 class="woocommerce-loop-product__title" ><?php the_title(); ?></h2>

    <span class="price <?php if(is_shop()) { echo " checking"; }?>">
        <ins><span class="woocommerce-Price-amount amount"><?php echo $product->get_price_html();?><?php if(is_front_page())?></span></ins>
    </span>
    <?php echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id ); ?>
</a>

    
<div class="product_brief_content">
<!--    <p>--><?php //the_field('short_detail');?><!--</p>-->
</div>
    <?php global $product;
    
   echo apply_filters( 'woocommerce_loop_add_to_cart_link', // WPCS: XSS ok.
	sprintf( '<a href="%s" data-quantity="%s" class="cartbtn" %s>IN DEN WARENKORB</a>',
	esc_url( $product->add_to_cart_url() ),
	esc_attr( isset( $args['quantity'] ) ? $args['quantity'] : 1 ),
		esc_attr( isset( $args['class'] ) ? $args['class'] : 'button' ),
		isset( $args['attributes'] ) ? wc_implode_html_attributes( $args['attributes'] ) : '',
		esc_html( $product->add_to_cart_text() )
	),
  $product, $args );
    ?>
<!--    <a href="<?php the_permalink(); ?>?add-to-cart=<?php echo get_the_ID();?>" class="btn" tabindex="0">in den Warenkorb</a>-->
    
    
    <a href="<?php the_permalink();?>" class="morebtn">ENTDECKEN SIE MEHR </a>
    

</li>
