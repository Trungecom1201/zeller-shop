<?php
/**
 * Product attributes
 *
 * Used by list_attributes() in the products class.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-attributes.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<table class="shop_attributes">
	<?php if ( $display_dimensions && $product->has_weight() ){ ?>
		<tr>
			<th><?php _e( 'Weight', 'woocommerce' ) ?></th>
			<td class="product_weight"><?php echo esc_html( wc_format_weight( $product->get_weight() ) ); ?></td>
		</tr>
	<?php } ?>

	<?php if ( $display_dimensions && $product->has_dimensions() ) { ?>
		<tr>
			<th><?php _e( 'Dimensions', 'woocommerce' ) ?></th>
			<td class="product_dimensions"><?php echo esc_html( wc_format_dimensions( $product->get_dimensions( false ) ) ); ?></td>
		</tr>
	<?php } ?>

	<?php foreach ( $attributes as $attribute ) : ?>
		<tr>
			<th><?php echo wc_attribute_label( $attribute->get_name() ); ?></th>
			<td><?php
				$values = array();

				if ( $attribute->is_taxonomy() ) {
					$attribute_taxonomy = $attribute->get_taxonomy_object();
					$attribute_values = wc_get_product_terms( $product->get_id(), $attribute->get_name(), array( 'fields' => 'all' ) );

					foreach ( $attribute_values as $attribute_value ) {
						$value_name = esc_html( $attribute_value->name );

						if ( $attribute_taxonomy->attribute_public ) {
							$values[] = '<a href="' . esc_url( get_term_link( $attribute_value->term_id, $attribute->get_name() ) ) . '" rel="tag">' . $value_name . '</a>';
						} else {
							$values[] = $value_name;
						}
					}
				} else {
					$values = $attribute->get_options();

					foreach ( $values as &$value ) {
						$value = make_clickable( esc_html( $value ) );
					}
				}

				echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );
			?></td>
		</tr>


	<?php endforeach; ?>

	 <?php /*$brand  = get_field('brand_'); if($brand) {?>
	<tr> <th>Marke / Hersteller </th> <td> <?php the_field('brand_');?> </td></tr>
    <?php } ?>
    <?php $material  = get_field('material'); if($material) {?>
	<tr> <th>Material </th> <td> <?php the_field('material');?> </td></tr>
    <?php } ?>

    <?php $ausfuhrung  = get_field('ausfuhrung'); if($ausfuhrung) {?>
	<tr> <th>Ausführung </th> <td> <?php the_field('ausfuhrung');?> </td></tr>
    <?php } ?>

    <?php $wellen  = get_field('wellen'); if($wellen) {?>
	<tr> <th>Wellen </th> <td> <?php the_field('wellen');?> </td></tr>
    <?php } ?>

    <?php $stellkraft  = get_field('stellkraft'); if($stellkraft) {?>
	<tr> <th>Stellkraft </th> <td> <?php the_field('stellkraft');?> </td></tr>
    <?php } ?>

    <?php $short_detail  = get_field('short_detail'); if($short_detail) {?>
	<tr> <th>Marke / Hersteller </th> <td> <?php the_field('short_detail');?> </td></tr>
    <?php } */?>




    <?php
		/*Produktset Flugmodelle*/
		$equipment  = get_field('equipment');
    $wingspan  = get_field('wingspan');
		$length_text  = get_field('length_text');
    $flightweight  = get_field('flightweight');
    $airfoil  = get_field('airfoil');
    $wing_area  = get_field('wing_area');
    $wing_loading  = get_field('wing_loading');
    $wingspan_categorie  = get_field('wingspan_categorie');
    $controls  = get_field('controls');
		$necessary_channel  = get_field('necessary_channel');
		$fuselage  = get_field('fuselage');
		$wing  = get_field('wing');
		$recommended_servos  = get_field('recommended_servos');
		$recommended_battery  = get_field('recommended_battery');
		$recommended_esc  = get_field('recommended_esc');
		$recommended_motor  = get_field('recommended_motor');
		$recommended_prop  = get_field('recommended_prop');

		/*Produktset Akkus & Batterien*/
		$batterie_typ  = get_field('batterie_typ');
		$volume  = get_field('volume');

		/*Produktset Beleuchtung*/
		$angle_of_radiation  = get_field('angle_of_radiation');
		$maxcurrent  = get_field('maxcurrent');
		$charge_power  = get_field('charge_power');

		/*Produktset E-Motore*/
		$typ  = get_field('typ');
		$u_v_category  = get_field('u_v_category');
		$u_v  = get_field('u_v');
		$maxcurrent  = get_field('maxcurrent');
		$max_efficiency  = get_field('max_efficiency');
		$internal_resistance  = get_field('internal_resistance');
		$wirewinds  = get_field('wirewinds');
		$power  = get_field('power');
		$thrust  = get_field('thrust');
		$axle_diameter  = get_field('axle_diameter');
		$axle_length  = get_field('axle_length');
		$hole_spacing  = get_field('hole_spacing');
		$recommended_use  = get_field('recommended_use');
		$recommended_prop  = get_field('recommended_prop');

		/*Produktset Ladegeräte*/
		$charger_category  = get_field('charger_category');
		$dc_input  = get_field('dc_input');
		$ac_input  = get_field('ac_input');
		$charge_power  = get_field('charge_power');
		$charge_current_range  = get_field('charge_current_range');
		$max_charge  = get_field('max_charge');
		$discharge_current_range  = get_field('discharge_current_range');
		$max_discharge  = get_field('max_discharge');
		$pb_voltage_range  = get_field('pb_voltage_range');

		/*Produktset LiPo*/
		$energy  = get_field('energy');
		$max_charge  = get_field('max_charge');
		$max_discharge  = get_field('max_discharge');
		$discharge_category  = get_field('discharge_category');

		/*Produktset Luftschrauben*/
		$prop_typ  = get_field('prop_typ');
		$prop_categorie  = get_field('prop_categorie');
		$drill_hole  = get_field('drill_hole');

		/*Produktset RC*/
		$channel  = get_field('channel');
		$rx_batt_connector  = get_field('rx_batt_connector');
		$antenna_length  = get_field('antenna_length');
		$antenna_number  = get_field('antenna_number');
		$telemetry  = get_field('telemetry');
		$satellite_receiver_support  = get_field('satellite_receiver_support');
		$programming  = get_field('programming');
		$voltagerange  = get_field('voltagerange');
		$current_consumption  = get_field('current_consumption');
		$power_output  = get_field('power_output');
		$receiver_sensitivity  = get_field('receiver_sensitivity');

		/*Produktset Regler*/
		$continuouscurrent_categorie  = get_field('continuouscurrent_categorie');
		$burstcurrent  = get_field('burstcurrent');
		$bec  = get_field('bec');

		/*Produktset Servos*/
		$servo_categorie  = get_field('servo_categorie');
		$servotyp  = get_field('servotyp');
		$gear  = get_field('gear');
		$gear_categorie  = get_field('gear_categorie');
		$ball_bearing  = get_field('ball_bearing');
		$ball_bearing_categorie  = get_field('ball_bearing_categorie');
		$torque  = get_field('torque');
		$torque_1  = get_field('torque_1');
		$torque_category  = get_field('torque_category');
		$speed  = get_field('speed');
		$speed_1  = get_field('speed_1');
		$hv_category  = get_field('hv_category');
		$motor_type  = get_field('motor_type');
		$idle_current  = get_field('idle_current');
		$runnig_current  = get_field('runnig_current');
		$stall_current  = get_field('stall_current');

		/*Produktset Zubehör*/
		$thread_categorie  = get_field('thread_categorie');
		$volume  = get_field('volume');

		/*Produktset Standard*/
		$brand_  = get_field('brand_');
		$nettogewicht_g  = get_field('nettogewicht_g');
		$diameter_categorie  = get_field('diameter_categorie');
		$width_text  = get_field('width_text');
		$height_text  = get_field('height_text');
		$diameter  = get_field('diameter');
		$color  = get_field('color');
		$voltagerange  = get_field('voltagerange');
		$capacity  = get_field('capacity');
		$cells_category  = get_field('cells_category');
		$continuouscurrent  = get_field('continuouscurrent');
		$axle_diameter_category  = get_field('axle_diameter_category');
		$capacity_category  = get_field('capacity_category');
		$connector  = get_field('connector');
		$balancer_connector  = get_field('balancer_connector');
		$temperature_range  = get_field('temperature_range');
		$material  = get_field('material');
		$material_categorie  = get_field('material_categorie');
		$lighting_type  = get_field('lighting_type');
		$nicdnimh  = get_field('nicdnimh');
		$lipo  = get_field('lipo');

    ?>

<!--Produktset Listung-->

<?php if($brand_) {?>
<tr> <th>Marke / Hersteller </th> <td> <?php echo $brand_;?> </td></tr>
<?php } ?>

<?php if($version) {?>
<tr> <th>Ausführung </th> <td> <?php echo $version;?> </td></tr>
<?php } ?>

<?php if($equipment) {?>
<tr> <th>Ausstattung/Lieferumfang </th> <td> <?php echo $equipment;?> </td></tr>
<?php } ?>

<?php if($volume) {?>
<tr> <th>Inhalt </th> <td> <?php echo $volume;?> </td></tr>
<?php } ?>

<?php if($diameter_categorie) {?>
<tr> <th>Durchmesser </th> <td> <?php echo $diameter_categorie;?> </td></tr>
<?php } ?>

<?php if($length_text) {?>
<tr> <th>Länge (mm) </th> <td> <?php echo $length_text;?> </td></tr>
<?php } ?>

<?php if($width_text) {?>
<tr> <th>Breite (mm) </th> <td> <?php echo $width_text;?> </td></tr>
<?php } ?>

<?php if($servo_categorie) {?>
<tr> <th>Breite </th> <td> <?php echo $servo_categorie;?> </td></tr>
<?php } ?>

<?php if($height_text) {?>
<tr> <th>Höhe (mm) </th> <td> <?php echo $height_text;?> </td></tr>
<?php } ?>

<?php if($diameter) {?>
<tr> <th>Ø (mm) </th> <td> <?php echo $diameter;?> </td></tr>
<?php } ?>

<?php if($wingspan) {?>
<tr> <th>Spannweite (mm) </th> <td> <?php echo $wingspan;?> </td></tr>
<?php } ?>

<?php if($flightweight) {?>
<tr> <th>Fluggewicht (g) </th> <td> <?php echo $flightweight;?> </td></tr>
<?php } ?>

<?php if($nettogewicht_g) {?>
<tr> <th>Nettogewicht (g)</th> <td> <?php echo $nettogewicht_g;?> </td></tr>
<?php } ?>

<?php if($airfoil) {?>
<tr> <th>Profil </th> <td> <?php echo $airfoil;?> </td></tr>
<?php } ?>

<?php if($wing_area) {?>
<tr> <th>Flächeninhalt (dm²) </th> <td> <?php echo $wing_area;?> </td></tr>
<?php } ?>

<?php if($wing_loading) {?>
<tr> <th>Flächenbelastung (g/dm²) </th> <td> <?php echo $wing_loading;?> </td></tr>
<?php } ?>

<?php if($color) {?>
<tr> <th>Farbe </th> <td> <?php echo $color;?> </td></tr>
<?php } ?>

<?php if($controls) {?>
<tr> <th>Funktionen </th> <td> <?php echo $controls;?> </td></tr>
<?php } ?>

<?php if($necessary_channel) {?>
<tr> <th>Notwendige Kanalanzahl </th> <td> <?php echo $necessary_channel;?> </td></tr>
<?php } ?>

<?php if($fuselage) {?>
<tr> <th>Rumpf </th> <td> <?php echo $fuselage;?> </td></tr>
<?php } ?>

<?php if($material) {?>
<tr> <th>Material </th> <td> <?php echo $material;?> </td></tr>
<?php } ?>

<?php if($material_categorie) {?>
<tr> <th>Material Kategorie </th> <td> <?php echo $material_categorie;?> </td></tr>
<?php } ?>

<?php if($prop_typ) {?>
<tr> <th>Propeller Typ </th> <td> <?php echo $prop_typ;?> </td></tr>
<?php } ?>

<?php if($prop_categorie) {?>
<tr> <th>Propeller Größe </th> <td> <?php echo $prop_categorie;?> </td></tr>
<?php } ?>

<?php if($wing) {?>
<tr> <th>Fläche </th> <td> <?php echo $wing;?> </td></tr>
<?php } ?>

<?php if($voltagerange) {?>
<tr> <th>Spannungsbereich (V) </th> <td> <?php echo $voltagerange;?> </td></tr>
<?php } ?>

<?php if($capacity) {?>
<tr> <th>Kapazität (mAh) </th> <td> <?php echo $capacity;?> </td></tr>
<?php } ?>

<?php if($cells_category) {?>
<tr> <th>Zellenanzahl (Spannung) </th> <td> <?php echo $cells_category;?> </td></tr>
<?php } ?>

<?php if($continuouscurrent) {?>
<tr> <th>Dauerstrom (A) </th> <td> <?php echo $continuouscurrent;?> </td></tr>
<?php } ?>

<?php if($axle_diameter_category) {?>
<tr> <th>Wellen Ø (mm) </th> <td> <?php echo $axle_diameter_category;?> </td></tr>
<?php } ?>

<?php if($capacity_category) {?>
<tr> <th>Kapazität </th> <td> <?php echo $capacity_category;?> </td></tr>
<?php } ?>

<?php if($connector) {?>
<tr> <th>Stecksystem </th> <td> <?php echo $connector;?> </td></tr>
<?php } ?>

<?php if($lighting_type) {?>
<tr> <th>Typ </th> <td> <?php echo $lighting_type;?> </td></tr>
<?php } ?>

<?php if($typ) {?>
<tr> <th>Motor-Typ </th> <td> <?php echo $typ;?> </td></tr>
<?php } ?>

<?php if($balancer_connector) {?>
<tr> <th>Balancer Stecker </th> <td> <?php echo $balancer_connector;?> </td></tr>
<?php } ?>

<?php if($temperature_range) {?>
<tr> <th>Temperaturbereich (°C) </th> <td> <?php echo $temperature_range;?> </td></tr>
<?php } ?>

<?php if($angle_of_radiation) {?>
<tr> <th>Abstrahlwinkel </th> <td> <?php echo $angle_of_radiation;?> </td></tr>
<?php } ?>

<?php if($maxcurrent) {?>
<tr> <th>Max. Stromaufnahme (A) </th> <td> <?php echo $maxcurrent;?> </td></tr>
<?php } ?>

<?php if($max_efficiency) {?>
<tr> <th>Max. Wirkungsgrad in % </th> <td> <?php echo $max_efficiency;?> </td></tr>
<?php } ?>

<?php if($charge_power) {?>
<tr> <th>Max. Leistung (W) </th> <td> <?php echo $charge_power;?> </td></tr>
<?php } ?>

<?php if($u_v_category) {?>
<tr> <th>Drehzahl / V (KV) </th> <td> <?php echo $u_v_category;?> </td></tr>
<?php } ?>

<?php if($u_v) {?>
<tr> <th>U/V </th> <td> <?php echo $u_v;?> </td></tr>
<?php } ?>

<?php if($internal_resistance) {?>
<tr> <th>Innenwiderstand mOhm </th> <td> <?php echo $internal_resistance;?> </td></tr>
<?php } ?>

<?php if($wirewinds) {?>
<tr> <th>Windungen </th> <td> <?php echo $wirewinds;?> </td></tr>
<?php } ?>

<?php if($power) {?>
<tr> <th>Max. Leistung (W) </th> <td> <?php echo $power;?> </td></tr>
<?php } ?>

<?php if($thrust) {?>
<tr> <th>Max. Schub (g) </th> <td> <?php echo $thrust;?> </td></tr>
<?php } ?>

<?php if($axle_diameter) {?>
<tr> <th>Wellen Ø (mm) </th> <td> <?php echo $axle_diameter;?> </td></tr>
<?php } ?>

<?php if($axle_length) {?>
<tr> <th>Wellenlänge (mm) </th> <td> <?php echo $axle_length;?> </td></tr>
<?php } ?>

<?php if($charger_category) {?>
<tr> <th>Stromversorgung </th> <td> <?php echo $charger_category;?> </td></tr>
<?php } ?>

<?php if($dc_input) {?>
<tr> <th>Eingangsspannung DC (V) </th> <td> <?php echo $dc_input;?> </td></tr>
<?php } ?>

<?php if($ac_input) {?>
<tr> <th>Eingangsspannung AC (V) </th> <td> <?php echo $ac_input;?> </td></tr>
<?php } ?>

<?php if($charge_power) {?>
<tr> <th>Max. Leistung (W) </th> <td> <?php echo $charge_power;?> </td></tr>
<?php } ?>

<?php if($charge_current_range) {?>
<tr> <th>Ladestrom (A) </th> <td> <?php echo $charge_current_range;?> </td></tr>
<?php } ?>

<?php if($energy) {?>
<tr> <th>Nennenergie (Wh) </th> <td> <?php echo $energy;?> </td></tr>
<?php } ?>

<?php if($max_charge) {?>
<tr> <th>Max. Ladestrom C </th> <td> <?php echo $max_charge;?> </td></tr>
<?php } ?>

<?php if($discharge_category) {?>
<tr> <th>Entladestrom </th> <td> <?php echo $discharge_category;?> </td></tr>
<?php } ?>

<?php if($discharge_current_range) {?>
<tr> <th>Entladestrom (A) </th> <td> <?php echo $discharge_current_range;?> </td></tr>
<?php } ?>

<?php if($max_discharge) {?>
<tr> <th>Entladestrom Dauer / Kurz C </th> <td> <?php echo $max_discharge;?> </td></tr>
<?php } ?>

<?php if($pb_voltage_range) {?>
<tr> <th>Bleiakku </th> <td> <?php echo $pb_voltage_range;?> </td></tr>
<?php } ?>

<?php if($nicdnimh) {?>
<tr> <th>Ni-xx </th> <td> <?php echo $nicdnimh;?> </td></tr>
<?php } ?>

<?php if($lipo) {?>
<tr> <th>LiPo </th> <td> <?php echo $lipo;?> </td></tr>
<?php } ?>

<?php if($batterie_typ) {?>
<tr> <th>Typ </th> <td> <?php echo $batterie_typ;?> </td></tr>
<?php } ?>

<?php if($volume) {?>
<tr> <th>Inhalt </th> <td> <?php echo $volume;?> </td></tr>
<?php } ?>

<?php if($drill_hole) {?>
<tr> <th>Bohrung (mm) </th> <td> <?php echo $drill_hole;?> </td></tr>
<?php } ?>

<?php if($hole_spacing) {?>
<tr> <th>Lochabstand bzw. Ø Bef.bohr. (mm) </th> <td> <?php echo $hole_spacing;?> </td></tr>
<?php } ?>

<?php if($thread_categorie) {?>
<tr> <th>Gewinde </th> <td> <?php echo $thread_categorie;?> </td></tr>
<?php } ?>

<?php if($channel) {?>
<tr> <th>Kanäle </th> <td> <?php echo $channel;?> </td></tr>
<?php } ?>

<?php if($rx_batt_connector) {?>
<tr> <th>Stecksystem Stromversorgung </th> <td> <?php echo $rx_batt_connector;?> </td></tr>
<?php } ?>

<?php if($antenna_length) {?>
<tr> <th>Antennenlänge (mm) </th> <td> <?php echo $antenna_length;?> </td></tr>
<?php } ?>

<?php if($antenna_number) {?>
<tr> <th>Antennenanzahl </th> <td> <?php echo $antenna_number;?> </td></tr>
<?php } ?>

<?php if($telemetry) {?>
<tr> <th>Telemetrie </th> <td> <?php echo $telemetry;?> </td></tr>
<?php } ?>

<?php if($satellite_receiver_support) {?>
<tr> <th>Satellitenempfängertauglich </th> <td> <?php echo $satellite_receiver_support;?> </td></tr>
<?php } ?>

<?php if($programming) {?>
<tr> <th>Programmierung </th> <td> <?php echo $programming;?> </td></tr>
<?php } ?>

<?php if($voltagerange) {?>
<tr> <th>Spannungsbereich (V) </th> <td> <?php echo $voltagerange;?> </td></tr>
<?php } ?>

<?php if($current_consumption) {?>
<tr> <th>Stromaufnahme (mA) </th> <td> <?php echo $current_consumption;?> </td></tr>
<?php } ?>

<?php if($power_output) {?>
<tr> <th>Ausgangsleistung (dBm) </th> <td> <?php echo $power_output;?> </td></tr>
<?php } ?>

<?php if($receiver_sensitivity) {?>
<tr> <th>Empfänger Empfindlichkeit (dBm) </th> <td> <?php echo $receiver_sensitivity;?> </td></tr>
<?php } ?>

<?php if($continuouscurrent_categorie) {?>
<tr> <th>Dauerstrom </th> <td> <?php echo $continuouscurrent_categorie;?> </td></tr>
<?php } ?>

<?php if($burstcurrent) {?>
<tr> <th>Max.Belastung (10 Sek.) in A </th> <td> <?php echo $burstcurrent;?> </td></tr>
<?php } ?>

<?php if($bec) {?>
<tr> <th>BEC </th> <td> <?php echo $bec;?> </td></tr>
<?php } ?>

<?php if($servotyp) {?>
<tr> <th>Servotyp </th> <td> <?php echo $servotyp;?> </td></tr>
<?php } ?>

<?php if($gear) {?>
<tr> <th>Getriebe </th> <td> <?php echo $gear;?> </td></tr>
<?php } ?>

<?php if($gear_categorie) {?>
<tr> <th>Getriebe Kategorie</th> <td> <?php echo $gear_categorie;?> </td></tr>
<?php } ?>

<?php if($ball_bearing) {?>
<tr> <th>Kugellager </th> <td> <?php echo $ball_bearing;?> </td></tr>
<?php } ?>

<?php if($ball_bearing_categorie) {?>
<tr> <th>Kugellager Kategorie</th> <td> <?php echo $ball_bearing_categorie;?> </td></tr>
<?php } ?>

<?php if($torque) {?>
<tr> <th>Stellkraft 4,8 / 6V (kg) </th> <td> <?php echo $torque;?> </td></tr>
<?php } ?>

<?php if($torque_1) {?>
<tr> <th>Stellkraft 7,4 / 8,4V (kg) </th> <td> <?php echo $torque_1;?> </td></tr>
<?php } ?>

<?php if($torque_category) {?>
<tr> <th>Stellkraft </th> <td> <?php echo $torque_category;?> </td></tr>
<?php } ?>

<?php if($speed) {?>
<tr> <th>Stellzeit 4,8 / 6V (Sec. / 60°) </th> <td> <?php echo $speed;?> </td></tr>
<?php } ?>

<?php if($speed_1) {?>
<tr> <th>Stellzeit 7,4 / 8,4V (Sec. / 60°) </th> <td> <?php echo $speed_1;?> </td></tr>
<?php } ?>

<?php if($hv_category) {?>
<tr> <th>HV </th> <td> <?php echo $hv_category;?> </td></tr>
<?php } ?>

<?php if($motor_type) {?>
<tr> <th>Motor </th> <td> <?php echo $motor_type;?> </td></tr>
<?php } ?>

<?php if($idle_current) {?>
<tr> <th>Leerstromaufnahme (mA) </th> <td> <?php echo $idle_current;?> </td></tr>
<?php } ?>

<?php if($runnig_current) {?>
<tr> <th>Stromaufnahme im Betrieb (mA) </th> <td> <?php echo $runnig_current;?> </td></tr>
<?php } ?>

<?php if($stall_current) {?>
<tr> <th>Blockierstromaufnahme (mA) </th> <td> <?php echo $stall_current;?> </td></tr>
<?php } ?>

<?php if($recommended_servos) {?>
<tr> <th>Servo Empfehlung </th> <td> <?php echo $recommended_servos;?> </td></tr>
<?php } ?>

<?php if($recommended_battery) {?>
<tr> <th>Akku Empfehlung </th> <td> <?php echo $recommended_battery;?> </td></tr>
<?php } ?>

<?php if($recommended_esc) {?>
<tr> <th>Regler Empfehlung </th> <td> <?php echo $recommended_esc;?> </td></tr>
<?php } ?>

<?php if($recommended_motor) {?>
<tr> <th>Motor Empfehlung </th> <td> <?php echo $recommended_motor;?> </td></tr>
<?php } ?>

<?php if($recommended_prop) {?>
<tr> <th>Luftschrauben Empfehlung </th> <td> <?php echo $recommended_prop;?> </td></tr>
<?php } ?>

<?php if($recommended_use) {?>
<tr> <th>Anwendungsempfehlung </th> <td> <?php echo $recommended_use;?> </td></tr>
<?php } ?>

</table>
