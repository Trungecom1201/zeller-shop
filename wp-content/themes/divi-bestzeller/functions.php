<?php

add_filter( 'login_headerurl', 'custom_loginlogo_url' );
function custom_loginlogo_url($url) {
	return get_bloginfo( 'url' );
}


add_action("login_head", "my_login_head");
function my_login_head() {
	echo "
	<style>
	body.login #login h1 a {
		background: url('".get_stylesheet_directory_uri()."/images/logo.png') no-repeat scroll center top transparent;
		height: 90px;
		width: auto;
		background-size : contain;
	}
    body{    background: #1B3A4E;}
    .login #backtoblog a, .login #nav a {
    text-decoration: none;
    color: #eac930!important;}
	</style>
	";
}

/*
ACF Options
- register the ACF options
*/
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Footer Settings',
		'menu_title'	=> 'Footer Settings',
		'menu_slug' 	=> 'theme-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}



add_action( 'after_setup_theme', 'setup_woocommerce_support' );

function setup_woocommerce_support()
{
add_theme_support('woocommerce');
}


add_action( 'after_setup_theme', 'yourtheme_setup' );

function yourtheme_setup() {
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );
}


function home_banner() {
ob_start();      ?>

   <div class="banner">
    <div class="banner-slider">

    <?php
    //query_posts(array('post_type'=>'product','posts_per_page'=>'5','order'=>'DESC'));
    //while(have_posts()) : the_post();

//    $shop_page = get_option( 'woocommerce_shop_page_id' );
    global $post;
    ?>
    <?php if(get_field('product_slider',$post->ID)) : while(the_repeater_field('product_slider',$post->ID)) : ?>
    <?php $posts = get_sub_field('choose_product',$post->ID);	if( $posts ): foreach( $posts as $post):  setup_postdata($post); ?>
    <div>

        <div class="banner-img" <?php echo get_the_ID();?> >
            <img src="<?php the_sub_field('bg_image');?>"/>
                <div class="banner_caption">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="banner-main-left-section">
                                    <?php $img = get_sub_field('product_img'); if($img) {?>
                                    <img src="<?php echo $img; ?>"/>
                                    <?php } ?>
                                </div>
                                <div class="banner-right-content">

                                    <?php
                                    global $woocommerce;
                                    $currency = get_woocommerce_currency_symbol();
                                    $price = get_post_meta( get_the_ID(), '_regular_price', true);
                                    $sale = get_post_meta( get_the_ID(), '_sale_price', true);
                                    ?>

                                    <?php if($sale) : ?>
                                        <h3><?php echo $currency; echo " "; echo number_format ($price , 2 , ',' , ','); ?></h3>
                                        <span>statt <?php echo $currency; echo number_format ($sale , 2 , ',' , ','); ?></span>
                                    <?php elseif($price) : ?>
                                        <h3><?php echo $currency; echo " "; echo number_format ($price , 2 , ',' , ','); ?></h3>
                                    <?php endif; ?>


                                        <h4><?php the_title(); ?></h4>
                                        <p><?php the_field('short_detail', $post->ID); ?></p>

                                    <div class="banner-btn-section">

                                    <a href="/?add-to-cart=<?php echo get_the_ID(); ?>" class="btn">in den Warenkorb</a>
                                    <a href="<?php the_permalink(); ?>" class="btn">ENTDECKEN SIE MEHR</a>
                                   </div>


                                </div>
                                   <div class="banner-btn-section btn-for-mob">
                                    <a href="/?add-to-cart=<?php echo get_the_ID(); ?>" class="btn">in den Warenkorb</a>
                                    <a href="<?php the_permalink(); ?>" class="btn">ENTDECKEN SIE MEHR</a>
                                   </div>

                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <?php 	endforeach;  wp_reset_postdata();	endif; ?>

    <?php endwhile;  endif; ?>
    <?php //endwhile; wp_reset_query(); ?>

    </div>
</div>

<?php $html = ob_get_clean();
  return $html;
    }
add_shortcode( 'banner_slider', 'home_banner' );



function shop_banner() {
ob_start();      ?>

   <div class="banner" >
    <div class="banner-slider">

    <?php
    //query_posts(array('post_type'=>'product','posts_per_page'=>'5','order'=>'DESC'));
    //while(have_posts()) : the_post();

    $shop_page = get_option( 'woocommerce_shop_page_id' );
    global $post;
    ?>
    <?php if(get_field('product_slider', $shop_page)) : while(the_repeater_field('product_slider', $shop_page)) : ?>
    <?php $posts = get_sub_field('choose_product');	if( $posts ): foreach( $posts as $post):  setup_postdata($post); ?>
    <div>
        <div class="banner-img">
            <img src="<?php the_sub_field('bg_image');?>"/>
                <div class="banner_caption">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="banner-main-left-section">
                                    <?php $img = get_sub_field('product_img'); if($img) {?>
                                    <img src="<?php echo $img; ?>"/>
                                    <?php } ?>
                                </div>
                                <div class="banner-right-content">

                                    <?php
                                    global $woocommerce;
                                    $currency = get_woocommerce_currency_symbol();
                                    $price = get_post_meta( get_the_ID(), '_regular_price', true);
                                    $sale = get_post_meta( get_the_ID(), '_sale_price', true);
                                    ?>

                                    <?php if($sale) : ?>
                                        <h3><?php echo $currency; echo " "; echo number_format ($price , 2 , ',' , ','); ?></h3>
                                        <span>statt <?php echo $currency; echo number_format ($sale , 2 , ',' , ','); ?></span>
                                    <?php elseif($price) : ?>
                                        <h3><?php echo $currency; echo " "; echo number_format ($price , 2 , ',' , ','); ?></h3>
                                    <?php endif; ?>

                                        <h4><?php the_title(); ?></h4>
                                        <p><?php the_field('short_detail', $post->ID); ?></p>

                                   <div class="banner-btn-section">
                                    <a href="/?add-to-cart=<?php echo get_the_ID(); ?>" class="btn">in den Warenkorb</a>
                                    <a href="<?php the_permalink(); ?>" class="btn">ENTDECKEN SIE MEHR</a>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <?php 	endforeach;  wp_reset_postdata();	endif; ?>

    <?php endwhile;  endif; ?>
    <?php //endwhile; wp_reset_query(); ?>

    </div>
</div>

<?php $html2 = ob_get_clean();
  return $html2;
    }
add_shortcode( 'shop_banner_slider', 'shop_banner' );


add_filter( 'add_to_cart_text', 'woo_custom_single_add_to_cart_text' );                // < 2.1
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_single_add_to_cart_text' );  // 2.1 +

function woo_custom_single_add_to_cart_text() {
    return __( 'In den Warenkorb', 'woocommerce' );
}


add_action( 'wp_enqueue_scripts', 'wcs_dequeue_quantity' );
function wcs_dequeue_quantity() {
    wp_dequeue_style( 'wcqi-css' );
}



add_action( 'wp_enqueue_scripts', 'wcqi_enqueue_polyfill' );
function wcqi_enqueue_polyfill() {
    wp_enqueue_script( 'wcqi-number-polyfill' );
}


// Function to add shortcode to display tax rates
function woocommerce_template_display_tax() {
global $product;
$tax_rates = WC_Tax::get_rates( $product->get_tax_class() );
if (!empty($tax_rates)) {
    $tax_rate = reset($tax_rates);
    echo sprintf(_x('Price without %.2f %% tax', 'Text for tax rate. %.2f =
    tax rate', 'wptheme.foundation'), $tax_rate['rate']);
    }
 }
add_shortcode('display_tax', 'woocommerce_template_display_tax');



///////////////////////////////////////// child theme construction ////////////////////////////////////////////////////////
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' , 10 );



function divi_remove_scripts() {
//    wp_dequeue_style( 'understrap-styles' );
//    wp_deregister_style( 'understrap-styles' );
//
//    wp_dequeue_script( 'understrap-scripts' );
//    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'divi_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' , 30  );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();
    wp_enqueue_style( 'bootstrap.min', get_stylesheet_directory_uri() . '/css/bootstrap.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_style( 'font-awesome.min', get_stylesheet_directory_uri() . '/css/font-awesome.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_style( 'slick-theme', get_stylesheet_directory_uri() . '/css/slick-theme.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_style( 'slick', get_stylesheet_directory_uri() . '/css/slick.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_style( 'nice-select', get_stylesheet_directory_uri() . '/css/nice-select.css', array(), $the_theme->get( 'Version' ) );



    wp_enqueue_script( 'jquery');
  //  wp_enqueue_script( 'jquery.min', get_stylesheet_directory_uri() . '/js/jquery.min.js', array(), $the_theme->get( 'Version' ), true );
    wp_enqueue_script( 'bootstrap.min', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array(), $the_theme->get( 'Version' ), true );
    wp_enqueue_script( 'slick.min', get_stylesheet_directory_uri() . '/js/slick.min.js', array(), $the_theme->get( 'Version' ), true );
    wp_enqueue_script( 'jquery.matchHeight', get_stylesheet_directory_uri() . '/js/jquery.matchHeight.js', array(), $the_theme->get( 'Version' ), true );
    wp_enqueue_script( 'xc-custom', get_stylesheet_directory_uri() . '/js/xc-custom.js', array(), $the_theme->get( 'Version' ), true );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}


add_action('wp_enqueue_scripts', 'load_child_style', 50);
function load_child_style() {
  //register & enqueue parent with new name
  $the_theme = wp_get_theme();
wp_register_style('child-style', get_stylesheet_uri(), array('parent-style'));
// wp_enqueue_style('child-style');
wp_enqueue_style( 'responsive', get_stylesheet_directory_uri() . '/css/responsive.css', array(), $the_theme->get( 'Version' ) );
}

function ws_scripts_and_styles() {
	wp_enqueue_style('custom-style', get_stylesheet_directory_uri() .'/css/custom.css');
}
add_action( 'wp_enqueue_scripts', 'ws_scripts_and_styles' );

function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'divi-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );




function zeller_notice_shipping() {
$shippingdays="3-8";
$shipping_counrtry = WC()->session->get('customer')['shipping_country'];
if ($shipping_counrtry=="AT"){$shippingdays="1-2";}
if ($shipping_counrtry=="DE"){$shippingdays="2-5";}
$zone1=array("SI","HR","BE","CZ","CS","HU","IT","LB","LU","NL","SK");
if (in_array($shipping_counrtry, $zone1)){
$shippingdays="2-5";
}
$zone2=array("DA","DK","FI","FR","SV","SE","GB","EN");
if (in_array($shipping_counrtry, $zone2)){
$shippingdays="3-8";
}
echo '<p class="shippingtime" style="font-weight:bold;">Bitte erlaube '.$shippingdays.' Werktage zur Lieferung nach Auftragsabwicklung.</p>';
}
add_action( 'woocommerce_after_order_notes', 'zeller_notice_shipping' );






/**
 * Rename product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {

	$tabs['description']['title'] = __( 'Artikelbeschreibung' );		// Rename the description tab
	//$tabs['additional_information']['title'] = __( 'Artikeldetails' );	// Rename the additional information tab
	return $tabs;

}




//Remove WooCommerce Tabs
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] );  	// Remove the additional information tab
    return $tabs;

}

/*************** Add a custom product data tab *********************/
add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {
	$tabs['artikelbeschreibung_tab'] = array(
		'title' 	=> __( 'Artikeldetails', 'woocommerce' ),
		'priority' 	=> 11,
		'callback' 	=> 'woo_new_product_tab_content1'
	);

	return $tabs;


}
// tab 1 **** Artikelbeschreibung ****
function woo_new_product_tab_content1() {
	global $product;
	echo '<h2>Artikeldetails</h2>';

	/*Produktset Flugmodelle*/
	$version  = get_field('version');
	$equipment  = get_field('equipment');
	$wingspan  = get_field('wingspan');
	$length_text  = get_field('length_text');
	$flightweight  = get_field('flightweight');
	$airfoil  = get_field('airfoil');
	$wing_area  = get_field('wing_area');
	$wing_loading  = get_field('wing_loading');
	$wingspan_categorie  = get_field('wingspan_categorie');
	$controls  = get_field('controls');
	$necessary_channel  = get_field('necessary_channel');
	$fuselage  = get_field('fuselage');
	$wing  = get_field('wing');
	$recommended_servos  = get_field('recommended_servos');
	$recommended_battery  = get_field('recommended_battery');
	$recommended_esc  = get_field('recommended_esc');
	$recommended_motor  = get_field('recommended_motor');
	$recommended_prop  = get_field('recommended_prop');

	/*Produktset Akkus & Batterien*/
	$batterie_typ  = get_field('batterie_typ');
	$volume  = get_field('volume');

	/*Produktset Beleuchtung*/
	$angle_of_radiation  = get_field('angle_of_radiation');
	$maxcurrent  = get_field('maxcurrent');
	$charge_power  = get_field('charge_power');

	/*Produktset E-Motore*/
	$typ  = get_field('typ');
	$u_v_category  = get_field('u_v_category');
	$u_v  = get_field('u_v');
	$max_efficiency  = get_field('max_efficiency');
	$internal_resistance  = get_field('internal_resistance');
	$wirewinds  = get_field('wirewinds');
	$power  = get_field('power');
	$thrust  = get_field('thrust');
	$axle_diameter  = get_field('axle_diameter');
	$axle_length  = get_field('axle_length');
	$hole_spacing  = get_field('hole_spacing');
	$recommended_use  = get_field('recommended_use');

	/*Produktset Ladegeräte*/
	$charger_category  = get_field('charger_category');
	$dc_input  = get_field('dc_input');
	$ac_input  = get_field('ac_input');
	$charge_current_range  = get_field('charge_current_range');
	$max_charge  = get_field('max_charge');
	$discharge_current_range  = get_field('discharge_current_range');
	$max_discharge  = get_field('max_discharge');
	$pb_voltage_range  = get_field('pb_voltage_range');

	/*Produktset LiPo*/
	$energy  = get_field('energy');
	$discharge_category  = get_field('discharge_category');

	/*Produktset Luftschrauben*/
	$prop_typ  = get_field('prop_typ');
	$prop_categorie  = get_field('prop_categorie');
	$drill_hole  = get_field('drill_hole');

	/*Produktset RC*/
	$channel  = get_field('channel');
	$rx_batt_connector  = get_field('rx_batt_connector');
	$antenna_length  = get_field('antenna_length');
	$antenna_number  = get_field('antenna_number');
	$telemetry  = get_field('telemetry');
	$satellite_receiver_support  = get_field('satellite_receiver_support');
	$programming  = get_field('programming');
	$current_consumption  = get_field('current_consumption');
	$power_output  = get_field('power_output');
	$receiver_sensitivity  = get_field('receiver_sensitivity');

	/*Produktset Regler*/
	$continuouscurrent_categorie  = get_field('continuouscurrent_categorie');
	$burstcurrent  = get_field('burstcurrent');
	$bec  = get_field('bec');

	/*Produktset Servos*/
	$servo_categorie  = get_field('servo_categorie');
	$servotyp  = get_field('servotyp');
	$gear  = get_field('gear');
	$gear_categorie  = get_field('gear_categorie');
	$ball_bearing  = get_field('ball_bearing');
	$ball_bearing_categorie  = get_field('ball_bearing_categorie');
	$torque  = get_field('torque');
	$torque_1  = get_field('torque_1');
	$torque_category  = get_field('torque_category');
	$speed  = get_field('speed');
	$speed_1  = get_field('speed_1');
	$hv_category  = get_field('hv_category');
	$motor_type  = get_field('motor_type');
	$idle_current  = get_field('idle_current');
	$runnig_current  = get_field('runnig_current');
	$stall_current  = get_field('stall_current');

	/*Produktset Zubehör*/
	$thread_categorie  = get_field('thread_categorie');

	/*Produktset Standard*/
	$brand_  = get_field('brand_');
	$nettogewicht_g  = get_field('nettogewicht_g');
	$diameter_categorie  = get_field('diameter_categorie');
	$width_text  = get_field('width_text');
	$height_text  = get_field('height_text');
	$diameter  = get_field('diameter');
	$color  = get_field('color');
	$voltagerange  = get_field('voltagerange');
	$capacity  = get_field('capacity');
	$cells_category  = get_field('cells_category');
	$continuouscurrent  = get_field('continuouscurrent');
	$axle_diameter_category  = get_field('axle_diameter_category');
	$capacity_category  = get_field('capacity_category');
	$connector  = get_field('connector');
	$balancer_connector  = get_field('balancer_connector');
	$temperature_range  = get_field('temperature_range');
	$material  = get_field('material');
	$material_categorie  = get_field('material_categorie');
	$lighting_type  = get_field('lighting_type');
	$nicdnimh  = get_field('nicdnimh');
	$lipo  = get_field('lipo');

	?>




<table class="shop_attributes">

	<!--Produktset Listung-->

	<?php if($brand_) {?>
	<tr> <th>Marke / Hersteller </th> <td> <?php echo $brand_;?> </td></tr>
	<?php } ?>

	<?php if($version) {?>
	<tr> <th>Ausführung </th> <td> <?php echo $version;?> </td></tr>
	<?php } ?>

	<?php if($equipment) {?>
	<tr> <th>Ausstattung/Lieferumfang </th> <td> <?php echo $equipment;?> </td></tr>
	<?php } ?>

	<?php if($volume) {?>
	<tr> <th>Inhalt </th> <td> <?php echo $volume;?> </td></tr>
	<?php } ?>

	<?php if($diameter_categorie) {?>
	<tr> <th>Durchmesser </th> <td> <?php echo $diameter_categorie;?> </td></tr>
	<?php } ?>

	<?php if($length_text) {?>
	<tr> <th>Länge (mm) </th> <td> <?php echo $length_text;?> </td></tr>
	<?php } ?>

	<?php if($width_text) {?>
	<tr> <th>Breite (mm) </th> <td> <?php echo $width_text;?> </td></tr>
	<?php } ?>

	<?php if($servo_categorie) {?>
	<tr> <th>Breite </th> <td> <?php echo $servo_categorie;?> </td></tr>
	<?php } ?>

	<?php if($height_text) {?>
	<tr> <th>Höhe (mm) </th> <td> <?php echo $height_text;?> </td></tr>
	<?php } ?>

	<?php if($diameter) {?>
	<tr> <th>Ø (mm) </th> <td> <?php echo $diameter;?> </td></tr>
	<?php } ?>

	<?php if($wingspan) {?>
	<tr> <th>Spannweite (mm) </th> <td> <?php echo $wingspan;?> </td></tr>
	<?php } ?>

	<?php if($flightweight) {?>
	<tr> <th>Fluggewicht (g) </th> <td> <?php echo $flightweight;?> </td></tr>
	<?php } ?>

	<?php if($nettogewicht_g) {?>
	<tr> <th>Nettogewicht (g)</th> <td> <?php echo $nettogewicht_g;?> </td></tr>
	<?php } ?>

	<?php if($airfoil) {?>
	<tr> <th>Profil </th> <td> <?php echo $airfoil;?> </td></tr>
	<?php } ?>

	<?php if($wing_area) {?>
	<tr> <th>Flächeninhalt (dm²) </th> <td> <?php echo $wing_area;?> </td></tr>
	<?php } ?>

	<?php if($wing_loading) {?>
	<tr> <th>Flächenbelastung (g/dm²) </th> <td> <?php echo $wing_loading;?> </td></tr>
	<?php } ?>

	<?php if($color) {?>
	<tr> <th>Farbe </th> <td> <?php echo $color;?> </td></tr>
	<?php } ?>

	<?php if($controls) {?>
	<tr> <th>Funktionen </th> <td> <?php echo $controls;?> </td></tr>
	<?php } ?>

	<?php if($necessary_channel) {?>
	<tr> <th>Notwendige Kanalanzahl </th> <td> <?php echo $necessary_channel;?> </td></tr>
	<?php } ?>

	<?php if($fuselage) {?>
	<tr> <th>Rumpf </th> <td> <?php echo $fuselage;?> </td></tr>
	<?php } ?>

	<?php if($material) {?>
	<tr> <th>Material </th> <td> <?php echo $material;?> </td></tr>
	<?php } ?>

	<?php if($material_categorie) {?>
	<tr> <th>Material Kategorie </th> <td> <?php echo $material_categorie;?> </td></tr>
	<?php } ?>

	<?php if($prop_typ) {?>
	<tr> <th>Propeller Typ </th> <td> <?php echo $prop_typ;?> </td></tr>
	<?php } ?>

	<?php if($prop_categorie) {?>
	<tr> <th>Propeller Größe </th> <td> <?php echo $prop_categorie;?> </td></tr>
	<?php } ?>

	<?php if($wing) {?>
	<tr> <th>Fläche </th> <td> <?php echo $wing;?> </td></tr>
	<?php } ?>

	<?php if($voltagerange) {?>
	<tr> <th>Spannungsbereich (V) </th> <td> <?php echo $voltagerange;?> </td></tr>
	<?php } ?>

	<?php if($capacity) {?>
	<tr> <th>Kapazität (mAh) </th> <td> <?php echo $capacity;?> </td></tr>
	<?php } ?>

	<?php if($cells_category) {?>
	<tr> <th>Zellenanzahl (Spannung) </th> <td> <?php echo $cells_category;?> </td></tr>
	<?php } ?>

	<?php if($continuouscurrent) {?>
	<tr> <th>Dauerstrom (A) </th> <td> <?php echo $continuouscurrent;?> </td></tr>
	<?php } ?>

	<?php if($continuouscurrent_categorie) {?>
	<tr> <th>Dauerstrom Kategorie</th> <td> <?php echo $continuouscurrent_categorie;?> </td></tr>
	<?php } ?>

	<?php if($axle_diameter_category) {?>
	<tr> <th>Wellen Ø (mm) </th> <td> <?php echo $axle_diameter_category;?> </td></tr>
	<?php } ?>

	<?php if($connector) {?>
	<tr> <th>Stecksystem </th> <td> <?php echo $connector;?> </td></tr>
	<?php } ?>

	<?php if($lighting_type) {?>
	<tr> <th>Typ </th> <td> <?php echo $lighting_type;?> </td></tr>
	<?php } ?>

	<?php if($typ) {?>
	<tr> <th>Motor-Typ </th> <td> <?php echo $typ;?> </td></tr>
	<?php } ?>

	<?php if($balancer_connector) {?>
	<tr> <th>Balancer Stecker </th> <td> <?php echo $balancer_connector;?> </td></tr>
	<?php } ?>

	<?php if($temperature_range) {?>
	<tr> <th>Temperaturbereich (°C) </th> <td> <?php echo $temperature_range;?> </td></tr>
	<?php } ?>

	<?php if($angle_of_radiation) {?>
	<tr> <th>Abstrahlwinkel </th> <td> <?php echo $angle_of_radiation;?> </td></tr>
	<?php } ?>

	<?php if($maxcurrent) {?>
	<tr> <th>Max. Stromaufnahme (A) </th> <td> <?php echo $maxcurrent;?> </td></tr>
	<?php } ?>

	<?php if($max_efficiency) {?>
	<tr> <th>Max. Wirkungsgrad in % </th> <td> <?php echo $max_efficiency;?> </td></tr>
	<?php } ?>

	<?php if($charge_power) {?>
	<tr> <th>Max. Leistung (W) </th> <td> <?php echo $charge_power;?> </td></tr>
	<?php } ?>

	<?php if($u_v) {?>
	<tr> <th>U/V </th> <td> <?php echo $u_v;?> </td></tr>
	<?php } ?>

	<?php if($u_v_category) {?>
	<tr> <th>Drehzahl / V (KV) </th> <td> <?php echo $u_v_category;?> </td></tr>
	<?php } ?>

	<?php if($internal_resistance) {?>
	<tr> <th>Innenwiderstand mOhm </th> <td> <?php echo $internal_resistance;?> </td></tr>
	<?php } ?>

	<?php if($wirewinds) {?>
	<tr> <th>Windungen </th> <td> <?php echo $wirewinds;?> </td></tr>
	<?php } ?>

	<?php if($power) {?>
	<tr> <th>Max. Leistung (W) </th> <td> <?php echo $power;?> </td></tr>
	<?php } ?>

	<?php if($thrust) {?>
	<tr> <th>Max. Schub (g) </th> <td> <?php echo $thrust;?> </td></tr>
	<?php } ?>

	<?php if($axle_diameter) {?>
	<tr> <th>Wellen Ø (mm) </th> <td> <?php echo $axle_diameter;?> </td></tr>
	<?php } ?>

	<?php if($axle_length) {?>
	<tr> <th>Wellenlänge (mm) </th> <td> <?php echo $axle_length;?> </td></tr>
	<?php } ?>

	<?php if($charger_category) {?>
	<tr> <th>Stromversorgung </th> <td> <?php echo $charger_category;?> </td></tr>
	<?php } ?>

	<?php if($dc_input) {?>
	<tr> <th>Eingangsspannung DC (V) </th> <td> <?php echo $dc_input;?> </td></tr>
	<?php } ?>

	<?php if($ac_input) {?>
	<tr> <th>Eingangsspannung AC (V) </th> <td> <?php echo $ac_input;?> </td></tr>
	<?php } ?>

	<?php if($charge_current_range) {?>
	<tr> <th>Ladestrom (A) </th> <td> <?php echo $charge_current_range;?> </td></tr>
	<?php } ?>

	<?php if($energy) {?>
	<tr> <th>Nennenergie (Wh) </th> <td> <?php echo $energy;?> </td></tr>
	<?php } ?>

	<?php if($max_charge) {?>
	<tr> <th>Max. Ladestrom C </th> <td> <?php echo $max_charge;?> </td></tr>
	<?php } ?>

	<?php if($discharge_category) {?>
	<tr> <th>Entladestrom </th> <td> <?php echo $discharge_category;?> </td></tr>
	<?php } ?>

	<?php if($discharge_current_range) {?>
	<tr> <th>Entladestrom (A) </th> <td> <?php echo $discharge_current_range;?> </td></tr>
	<?php } ?>

	<?php if($max_discharge) {?>
	<tr> <th>Entladestrom Dauer / Kurz C </th> <td> <?php echo $max_discharge;?> </td></tr>
	<?php } ?>

	<?php if($pb_voltage_range) {?>
	<tr> <th>Bleiakku </th> <td> <?php echo $pb_voltage_range;?> </td></tr>
	<?php } ?>

	<?php if($nicdnimh) {?>
	<tr> <th>Ni-xx </th> <td> <?php echo $nicdnimh;?> </td></tr>
	<?php } ?>

	<?php if($lipo) {?>
	<tr> <th>LiPo </th> <td> <?php echo $lipo;?> </td></tr>
	<?php } ?>

	<?php if($batterie_typ) {?>
	<tr> <th>Typ </th> <td> <?php echo $batterie_typ;?> </td></tr>
	<?php } ?>

	<?php if($drill_hole) {?>
	<tr> <th>Bohrung (mm) </th> <td> <?php echo $drill_hole;?> </td></tr>
	<?php } ?>

	<?php if($hole_spacing) {?>
	<tr> <th>Lochabstand bzw. Ø Bef.bohr. (mm) </th> <td> <?php echo $hole_spacing;?> </td></tr>
	<?php } ?>

	<?php if($thread_categorie) {?>
	<tr> <th>Gewinde </th> <td> <?php echo $thread_categorie;?> </td></tr>
	<?php } ?>

	<?php if($channel) {?>
	<tr> <th>Kanäle </th> <td> <?php echo $channel;?> </td></tr>
	<?php } ?>

	<?php if($rx_batt_connector) {?>
	<tr> <th>Stecksystem Stromversorgung </th> <td> <?php echo $rx_batt_connector;?> </td></tr>
	<?php } ?>

	<?php if($antenna_length) {?>
	<tr> <th>Antennenlänge (mm) </th> <td> <?php echo $antenna_length;?> </td></tr>
	<?php } ?>

	<?php if($antenna_number) {?>
	<tr> <th>Antennenanzahl </th> <td> <?php echo $antenna_number;?> </td></tr>
	<?php } ?>

	<?php if($telemetry) {?>
	<tr> <th>Telemetrie </th> <td> <?php echo $telemetry;?> </td></tr>
	<?php } ?>

	<?php if($satellite_receiver_support) {?>
	<tr> <th>Satellitenempfängertauglich </th> <td> <?php echo $satellite_receiver_support;?> </td></tr>
	<?php } ?>

	<?php if($programming) {?>
	<tr> <th>Programmierung </th> <td> <?php echo $programming;?> </td></tr>
	<?php } ?>

	<?php if($current_consumption) {?>
	<tr> <th>Stromaufnahme (mA) </th> <td> <?php echo $current_consumption;?> </td></tr>
	<?php } ?>

	<?php if($power_output) {?>
	<tr> <th>Ausgangsleistung (dBm) </th> <td> <?php echo $power_output;?> </td></tr>
	<?php } ?>

	<?php if($receiver_sensitivity) {?>
	<tr> <th>Empfänger Empfindlichkeit (dBm) </th> <td> <?php echo $receiver_sensitivity;?> </td></tr>
	<?php } ?>

	<?php if($burstcurrent) {?>
	<tr> <th>Max.Belastung (10 Sek.) in A </th> <td> <?php echo $burstcurrent;?> </td></tr>
	<?php } ?>

	<?php if($bec) {?>
	<tr> <th>BEC </th> <td> <?php echo $bec;?> </td></tr>
	<?php } ?>

	<?php if($servotyp) {?>
	<tr> <th>Servotyp </th> <td> <?php echo $servotyp;?> </td></tr>
	<?php } ?>

	<?php if($gear) {?>
	<tr> <th>Getriebe </th> <td> <?php echo $gear;?> </td></tr>
	<?php } ?>

	<?php if($gear_categorie) {?>
	<tr> <th>Getriebe Kategorie</th> <td> <?php echo $gear_categorie;?> </td></tr>
	<?php } ?>

	<?php if($ball_bearing) {?>
	<tr> <th>Kugellager </th> <td> <?php echo $ball_bearing;?> </td></tr>
	<?php } ?>

	<?php if($ball_bearing_categorie) {?>
	<tr> <th>Kugellager Kategorie</th> <td> <?php echo $ball_bearing_categorie;?> </td></tr>
	<?php } ?>

	<?php if($torque) {?>
	<tr> <th>Stellkraft 4,8 / 6V (kg) </th> <td> <?php echo $torque;?> </td></tr>
	<?php } ?>

	<?php if($torque_1) {?>
	<tr> <th>Stellkraft 7,4 / 8,4V (kg) </th> <td> <?php echo $torque_1;?> </td></tr>
	<?php } ?>

	<?php if($torque_category) {?>
	<tr> <th>Stellkraft </th> <td> <?php echo $torque_category;?> </td></tr>
	<?php } ?>

	<?php if($speed) {?>
	<tr> <th>Stellzeit 4,8 / 6V (Sec. / 60°) </th> <td> <?php echo $speed;?> </td></tr>
	<?php } ?>

	<?php if($speed_1) {?>
	<tr> <th>Stellzeit 7,4 / 8,4V (Sec. / 60°) </th> <td> <?php echo $speed_1;?> </td></tr>
	<?php } ?>

	<?php if($hv_category) {?>
	<tr> <th>HV </th> <td> <?php echo $hv_category;?> </td></tr>
	<?php } ?>

	<?php if($motor_type) {?>
	<tr> <th>Motor </th> <td> <?php echo $motor_type;?> </td></tr>
	<?php } ?>

	<?php if($idle_current) {?>
	<tr> <th>Leerstromaufnahme (mA) </th> <td> <?php echo $idle_current;?> </td></tr>
	<?php } ?>

	<?php if($runnig_current) {?>
	<tr> <th>Stromaufnahme im Betrieb (mA) </th> <td> <?php echo $runnig_current;?> </td></tr>
	<?php } ?>

	<?php if($stall_current) {?>
	<tr> <th>Blockierstromaufnahme (mA) </th> <td> <?php echo $stall_current;?> </td></tr>
	<?php } ?>

	<?php if($recommended_servos) {?>
	<tr> <th>Servo Empfehlung </th> <td> <?php echo $recommended_servos;?> </td></tr>
	<?php } ?>

	<?php if($recommended_battery) {?>
	<tr> <th>Akku Empfehlung </th> <td> <?php echo $recommended_battery;?> </td></tr>
	<?php } ?>

	<?php if($recommended_esc) {?>
	<tr> <th>Regler Empfehlung </th> <td> <?php echo $recommended_esc;?> </td></tr>
	<?php } ?>

	<?php if($recommended_motor) {?>
	<tr> <th>Motor Empfehlung </th> <td> <?php echo $recommended_motor;?> </td></tr>
	<?php } ?>

	<?php if($recommended_prop) {?>
	<tr> <th>Luftschrauben Empfehlung </th> <td> <?php echo $recommended_prop;?> </td></tr>
	<?php } ?>

	<?php if($recommended_use) {?>
	<tr> <th>Anwendungsempfehlung </th> <td> <?php echo $recommended_use;?> </td></tr>
	<?php } ?>

</table>
<?php }
